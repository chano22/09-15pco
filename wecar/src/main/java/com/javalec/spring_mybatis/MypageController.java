package com.javalec.spring_mybatis;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.javalec.spring_mybatis.dao.IDao;
import com.javalec.spring_mybatis.dto.AreaDto;
import com.javalec.spring_mybatis.dto.LicenceDto;
import com.javalec.spring_mybatis.dto.User_MemberDto;
import com.javalec.spring_mybatis.Sessioncheck;

@Controller

public class MypageController {
	
	@Autowired
	private SqlSession sqlSession;
	@Autowired
	private HttpSession session;
	@Autowired
	Sessioncheck checksession;
	
	@RequestMapping(value = "mypage")
	public ModelAndView mypagetop(HttpServletRequest req, HttpServletResponse resp)throws Exception{
		System.out.println("mypage");
		
		
	
		ModelAndView mav = new ModelAndView();
		IDao dao = sqlSession.getMapper(IDao.class);
	
		session = req.getSession();
	    if(session.getAttribute("dto") != null){
	    	User_MemberDto mdto = (User_MemberDto)session.getAttribute("dto");
	  	   	System.out.println(session.getAttribute("dto"));
	    	List<AreaDto> area = new ArrayList<AreaDto>();
			area = dao.listarea();
			mav.addObject("area", area);
			LicenceDto ldto = dao.getLicence(mdto.getId());
			mav.addObject("ldto", ldto);
			
			mav.setViewName("jsp/mypage/mypage");

			
		}else{
			mav=checksession.checksession(mav);
	
		}
	    return mav;
		
	}

	

}
