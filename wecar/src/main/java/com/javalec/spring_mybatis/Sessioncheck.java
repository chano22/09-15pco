package com.javalec.spring_mybatis;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

import com.javalec.spring_mybatis.dto.User_MemberDto;

@Controller
public class Sessioncheck {
	@Autowired
	private SqlSession sqlSession;
	
	
	public ModelAndView checksession(ModelAndView mav) {
		
		String msg = "null";
		String url = "null";
	   	msg = "로그인 후 사용가능합니다.로그인페이지로 이동합니다.";
			url ="login";
			mav.addObject("msg", msg);
			mav.addObject("url", url);
			mav.setViewName("message");
			return mav;
	}
	
	
}
