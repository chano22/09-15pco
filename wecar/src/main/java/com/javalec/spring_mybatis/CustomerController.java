package com.javalec.spring_mybatis;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.javalec.spring_mybatis.dao.IDao;
import com.javalec.spring_mybatis.dto.Paging;
import com.javalec.spring_mybatis.dto.User_MemberDto;
import com.javalec.spring_mybatis.dto.mtomListDto;

@Controller
public class CustomerController {

	@Autowired
	private SqlSession sqlSession;
	@Autowired
	Sessioncheck checksession;

	@Autowired
	private HttpSession session;
	

	
	@RequestMapping(value = "notice", method=RequestMethod.GET)	
	public ModelAndView notice(HttpServletRequest request, Model model) throws Exception {
		try {
			System.out.println("noitce 접속");
			ModelAndView mav = new ModelAndView();

			if (session.getAttribute("dto") != null) {
				mav.setViewName("jsp/customer/notice");
			} else {
				mav = checksession.checksession(mav);

			}
			
			
			
			IDao dao = sqlSession.getMapper(IDao.class);
			model.addAttribute("noticeDto", dao.noticelistDao());
			
			System.out.println("PAGING 처리");
			
	        Paging paging = new Paging();
	        paging.setTotalCount(dao.noticelistCount());
	        paging.setPageNo(1);
	        paging.setPageSize(5);

			model.addAttribute("paging", paging);
			System.out.println(paging.toString());
			
			session = request.getSession();
			User_MemberDto mdto = null;
			mdto = (User_MemberDto)session.getAttribute("dto");
			
			if(mdto != null) {		// 로그인 상태일때
				System.out.println("Current ID : " + mdto.getId());
				model.addAttribute("CurrentID", mdto.getId());
				if(mdto.getId().equals("admin"))
					System.out.println("글쓰기 버튼 활성화");
			}
			else {
				System.out.println("Current ID : NULL");
			}
			
			System.out.println();

			return mav;
			}
		catch(Exception e) {
			throw e;
		}
	}
	
	@RequestMapping(value = "noticeDetail", method=RequestMethod.GET)
	public String noticeDetail(HttpServletRequest request, Model model) throws Exception {
		try {
			System.out.println("NOTICE DETAIL PAGE");
			System.out.println("NO : " + (String)request.getParameter("no"));
		
			IDao dao = sqlSession.getMapper(IDao.class);
			model.addAttribute("noticeDetailDto", dao.noticeDetailDao(Integer.parseInt((String)request.getParameter("no"))));
			
			System.out.println();
			return "jsp/customer/noticeDetail";
		}
		catch (Exception e) {
			throw e;
		}
	}
	
	@RequestMapping(value = "noticeWrite", method=RequestMethod.GET)
	public String noticeWrite(HttpServletRequest request, Model model) {
		System.out.println("NOTICE Write PAGE");
		
		IDao dao = sqlSession.getMapper(IDao.class);
		model.addAttribute("noticeCategoryDto", dao.noticeCategoryDao());
		
		System.out.println();
		return "jsp/customer/noticeWrite";
	}
	
	@RequestMapping(value = "noticeWriteOK", method=RequestMethod.GET)
	public String noticeWriteOK(HttpServletRequest request, Model model) {
		System.out.println("NOTICE LIST로 돌아갑니다");
		
		IDao dao = sqlSession.getMapper(IDao.class);
		dao.noticeWriteDao(Integer.parseInt(request.getParameter("category")), request.getParameter("subject"), request.getParameter("contents"));

		System.out.println("category : " + request.getParameter("category") + "/ subject : " + request.getParameter("subject") + "/ contents : " + request.getParameter("contents"));
		
		System.out.println();
		return "redirect:notice";
	}
	
	@RequestMapping(value = "question", method=RequestMethod.GET)
	public String question(HttpServletRequest request, Model model) {
		System.out.println("QNA PAGE");
		
		IDao dao = sqlSession.getMapper(IDao.class);
		model.addAttribute("qnaDto", dao.qnalistDao());
		model.addAttribute("qnaCategoryDto", dao.qnaCategoryDao());
		
		session = request.getSession();
		User_MemberDto mdto = null;
		mdto = (User_MemberDto)session.getAttribute("dto");
		
		if(mdto != null) {		// 로그인 상태일때
			System.out.println("Current ID : " + mdto.getId());
			model.addAttribute("CurrentID", mdto.getId());
			if(mdto.getId().equals("admin"))
				System.out.println("글쓰기 버튼 활성화");
		}
		else {
			System.out.println("Current ID : NULL");
		}
		
		System.out.println();
		return "jsp/customer/question";
	}
	
	@RequestMapping(value = "questionWrite", method=RequestMethod.GET)
	public String questionWrite(HttpServletRequest request, Model model) {
		System.out.println("QNA Write PAGE");
		
		System.out.println();
		return "jsp/customer/questionWrite";
	}
	
	@RequestMapping(value = "questionWriteOK", method=RequestMethod.GET)
	public String questionWriteOK(HttpServletRequest request, Model model) {
		System.out.println("QNA LIST로 돌아갑니다");
		
		System.out.println();
		return "redirect:question";
	}
	
	@RequestMapping(value = "mantoman", method=RequestMethod.GET)
	public ModelAndView mantoman(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView();
		
		System.out.println("1:1 상담 PAGE");
		
		IDao dao = sqlSession.getMapper(IDao.class);
		
		session = request.getSession();
		User_MemberDto mdto = null;
		mdto = (User_MemberDto)session.getAttribute("dto");
		
		if(mdto != null) {		// 로그인 상태일때
			System.out.println("Current ID : " + mdto.getId());
			
			if(mdto.getId().equals("admin")) {
				System.out.println("글쓰기 버튼 비활성화");
				mav.addObject("mantomanDto", dao.mtomListDao2());
				mav.addObject("CurrentID", mdto.getId());
			}
			else {
				mav.addObject("mantomanDto", dao.mtomListDao(mdto.getId()));
				mav.addObject("CurrentID", mdto.getId());
			}
			
			mav.setViewName("jsp/customer/mantoman");
		}
		else {						// 비로그인 상태일때
			String msg = "로그인이 필요한 서비스 입니다.";
			String url = "login";
			mav.addObject("msg", msg);
			mav.addObject("url", url);
			mav.setViewName("message");
			
			System.out.println("login 필요");
			
			System.out.println();
			return mav;
		}
			
		System.out.println("PAGING 처리");
		
        Paging paging = new Paging();
        paging.setTotalCount(dao.mtomlistCount());
        paging.setPageNo(1);
        paging.setPageSize(5);

		mav.addObject("paging2", paging);
		System.out.println(paging.toString());
		
		System.out.println();
		return mav;
	}
	
	@RequestMapping(value = "mantomanDetail", method=RequestMethod.GET)
	public ModelAndView mantomanDetail(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView();
		System.out.println("1:1 상담 DETAIL PAGE");
		
		IDao dao = sqlSession.getMapper(IDao.class);
		
		session = request.getSession();
		User_MemberDto mdto = null;
		mdto = (User_MemberDto)session.getAttribute("dto");
		
		if(mdto == null) {						// 비로그인 상태일때
			String msg = "로그인이 필요한 서비스 입니다.";
			String url = "login";
			mav.addObject("msg", msg);
			mav.addObject("url", url);
			mav.setViewName("message");
			
			System.out.println("login 필요");
			
			System.out.println();
			return mav;
		}
		
		System.out.println("no : " + Integer.parseInt(request.getParameter("no")));
		
		mav.addObject("mtomDetailDto", dao.mtomDetailDao(Integer.parseInt(request.getParameter("no"))));
		mav.setViewName("jsp/customer/mantomanDetail");
		
		System.out.println();
		return mav;
	}
	
	@ResponseBody		// json 댓글 테스트
    @RequestMapping(value = "comment", method=RequestMethod.POST, produces="text/plain;charset=UTF-8")
    public void doF(HttpServletRequest request, HttpServletResponse response) {
		session = request.getSession();
		User_MemberDto mdto = null;
		mdto = (User_MemberDto)session.getAttribute("dto");
		
		//System.out.println("ID : " + request.getParameter("id") + " / no : " + mtomList.getCategory());
		//System.out.println("Contents : " + request.getParameter("textarea") + " / Filename : " + mtomList.getFilename());
		
		
		System.out.println("댓글 등록");
    }

	
	@RequestMapping(value = "mantomanWrite", method=RequestMethod.GET)
	public String mantomanWrite(HttpServletRequest request, Model model) {
		System.out.println("1:1 상담 글쓰기 페이지");
		
		IDao dao = sqlSession.getMapper(IDao.class);
		model.addAttribute("qnaCategoryDto", dao.qnaCategoryDao());
		
		System.out.println();
		return "jsp/customer/mantomanWrite";
	}
	
	@RequestMapping(value = "mantomanWriteOK", method=RequestMethod.GET)
	public String mantomanWriteOK(HttpServletRequest request, Model model) {
		System.out.println("글쓰기 완료");
		
		mtomListDto mtomList = new mtomListDto();
		mtomList.setId(request.getParameter("id"));
		mtomList.setCategory(request.getParameter("category"));	
		mtomList.setSubject(request.getParameter("subject"));
		mtomList.setContents(request.getParameter("message"));
		mtomList.setFilename(request.getParameter("File"));
		
		System.out.println("DB 연결 전");
		System.out.println("ID : " + mtomList.getId() + " / Category : " + mtomList.getCategory() + " / Subject : " + mtomList.getSubject());
		System.out.println("Contents : " + mtomList.getContents() + " / Filename : " + mtomList.getFilename());
		
		IDao dao = sqlSession.getMapper(IDao.class);
		dao.mtomWriteDao(mtomList.getId(), mtomList.getCategory(), mtomList.getSubject(), mtomList.getContents(), mtomList.getFilename());

		System.out.println("DB 연결 성공");
		System.out.println("1:1 상담 LIST로 돌아갑니다");
		
		System.out.println();
		return "redirect:mantoman";
	}
	
}
