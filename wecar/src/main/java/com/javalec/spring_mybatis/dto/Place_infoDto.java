package com.javalec.spring_mybatis.dto;

public class Place_infoDto {

	private String place_index;
	private String place_x;
	private String place_y;
	
	
	
	public String getPlace_index() {
		return place_index;
	}
	public void setPlace_index(String place_index) {
		this.place_index = place_index;
	}
	public String getPlace_x() {
		return place_x;
	}
	public void setPlace_x(String place_x) {
		this.place_x = place_x;
	}
	public String getPlace_y() {
		return place_y;
	}
	public void setPlace_y(String place_y) {
		this.place_y = place_y;
	}

}
