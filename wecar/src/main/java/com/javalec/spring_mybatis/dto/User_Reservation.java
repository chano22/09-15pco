package com.javalec.spring_mybatis.dto;

public class User_Reservation {

	
	private int user_reservation_index;
	private String user_id;
	private int reservation_final_index;
	private String min_time;
	private String max_time;
	private int price;
	
	
	public int getUser_reservation_index() {
		return user_reservation_index;
	}
	public void setUser_reservation_index(int user_reservation_index) {
		this.user_reservation_index = user_reservation_index;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public int getReservation_final_index() {
		return reservation_final_index;
	}
	public void setReservation_final_index(int reservation_final_index) {
		this.reservation_final_index = reservation_final_index;
	}
	public String getMin_time() {
		return min_time;
	}
	public void setMin_time(String min_time) {
		this.min_time = min_time;
	}
	public String getMax_time() {
		return max_time;
	}
	public void setMax_time(String max_time) {
		this.max_time = max_time;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
}
