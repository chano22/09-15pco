package com.javalec.spring_mybatis.dto;

public class mtomListDto {
	
	private String id;
	private int no;
	private String category;		// 카테고리명
	private String subject;
	private String contents;
	private String writeday;
	private String filename;

	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getContents() {
		return contents;
	}
	public void setContents(String contents) {
		this.contents = contents;
	}
	public String getWriteday() {
		return writeday;
	}
	public void setWriteday(String writeday) {
		this.writeday = writeday;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	@Override
	public String toString() {
		return "mtomListDto [id=" + id + ", no=" + no + ", category=" + category + ", subject=" + subject
				+ ", contents=" + contents + ", writeday=" + writeday + ", filename=" + filename + "]";
	}
	
	
}
