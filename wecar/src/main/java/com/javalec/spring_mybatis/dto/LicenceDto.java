package com.javalec.spring_mybatis.dto;

import java.util.Date;

public class LicenceDto {
      private String id;
      private String type;
      private int area;
      private String num1;
      private String num2;
      private String num3;
      private String issue_date;
      private String exp_date;
      private String birthday;
      private String gender;
      public String getId() {
         return id;
      }
      public void setId(String id) {
         this.id = id;
      }
      public String getType() {
         return type;
      }
      public void setType(String type) {
         this.type = type;
      }
      public int getArea() {
         return area;
      }
      public void setArea(int area) {
         this.area = area;
      }
      public String getNum1() {
         return num1;
      }
      public void setNum1(String num1) {
         this.num1 = num1;
      }
      public String getNum2() {
         return num2;
      }
      public void setNum2(String num2) {
         this.num2 = num2;
      }
      public String getNum3() {
         return num3;
      }
      public void setNum3(String num3) {
         this.num3 = num3;
      }
      
      
      public String getIssue_date() {
         return issue_date;
      }
      public void setIssue_date(String issue_date) {
         this.issue_date = issue_date;
      }
      public String getExp_date() {
         return exp_date;
      }
      public void setExp_date(String exp_date) {
         this.exp_date = exp_date;
      }
      public String getBirthday() {
         return birthday;
      }
      public void setBirthday(String birthday) {
         this.birthday = birthday;
      }
      public String getGender() {
         return gender;
      }
      public void setGender(String gender) {
         this.gender = gender;
      }
}