package com.javalec.spring_mybatis.dto;

public class noticeDto {

	private int no;
	private String name;		// category
	private String subject;
	private String contents;
	private String writeday;
	
	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getContents() {
		return contents;
	}
	public void setContents(String contents) {
		this.contents = contents;
	}
	public String getWriteday() {
		return writeday;
	}
	public void setWriteday(String writeday) {
		this.writeday = writeday;
	}
	
	@Override
	public String toString() {
		return "noticeDto [no=" + no + ", name=" + name + ", subject=" + subject + ", contents=" + contents
				+ ", writeday=" + writeday + "]";
	}
}
