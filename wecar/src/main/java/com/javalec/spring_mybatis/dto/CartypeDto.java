package com.javalec.spring_mybatis.dto;

public class CartypeDto {

	
	
	private String car_index ;
	  private String car_name ;
	  private String car_code ;
	  private String car_price;
	  private String car_oil_type;
	  private String car_option ;
	
	
	  public String getCar_index() {
		return car_index;
	}
	public void setCar_index(String car_index) {
		this.car_index = car_index;
	}
	public String getCar_name() {
		return car_name;
	}
	public void setCar_name(String car_name) {
		this.car_name = car_name;
	}
	public String getCar_code() {
		return car_code;
	}
	public void setCar_code(String car_code) {
		this.car_code = car_code;
	}
	public String getCar_price() {
		return car_price;
	}
	public void setCar_price(String car_price) {
		this.car_price = car_price;
	}
	public String getCar_oil_type() {
		return car_oil_type;
	}
	public void setCar_oil_type(String car_oil_type) {
		this.car_oil_type = car_oil_type;
	}
	public String getCar_option() {
		return car_option;
	}
	public void setCar_option(String car_option) {
		this.car_option = car_option;
	}
	
}
