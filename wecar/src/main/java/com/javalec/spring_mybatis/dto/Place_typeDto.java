package com.javalec.spring_mybatis.dto;

public class Place_typeDto {
	private String place_type_index;
	   private String place_type_name;  

	   public String getPlace_type_index() {
		return place_type_index;
	}
	public void setPlace_type_index(String place_type_index) {
		this.place_type_index = place_type_index;
	}
	public String getPlace_type_name() {
		return place_type_name;
	}
	public void setPlace_type_name(String place_type_name) {
		this.place_type_name = place_type_name;
	}

}
