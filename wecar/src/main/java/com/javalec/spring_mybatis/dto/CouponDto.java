package com.javalec.spring_mybatis.dto;

public class CouponDto {

	public int no;
	public String id;
	public String exp_date1;//유효기간 (시작)
	public String exp_date2; //유효기간(끝)
	public String state;
	
	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getExp_date1() {
		return exp_date1;
	}
	public void setExp_date1(String exp_date1) {
		this.exp_date1 = exp_date1;
	}
	public String getExp_date2() {
		return exp_date2;
	}
	public void setExp_date2(String exp_date2) {
		this.exp_date2 = exp_date2;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	
}
