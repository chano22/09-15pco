package com.javalec.spring_mybatis.dto;

public class Coupon_typeDto {

	public int no; //쿠폰번호 시퀀스
	public String subject; //쿠폰할인내용
	public String exp_date1;//쿠폰유효기간(시작)
	public String exp_date2;//쿠폰유효기간(끝)
	public String operation; //+,-,x,/등 수식기호
	public int num;//수식을 이용할 숫자, 할인숫자
	
	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getExp_date1() {
		return exp_date1;
	}
	public void setExp_date1(String exp_date1) {
		this.exp_date1 = exp_date1;
	}
	public String getExp_date2() {
		return exp_date2;
	}
	public void setExp_date2(String exp_date2) {
		this.exp_date2 = exp_date2;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	
	
	
}
