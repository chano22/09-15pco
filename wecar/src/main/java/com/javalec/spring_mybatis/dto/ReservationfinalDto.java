package com.javalec.spring_mybatis.dto;

public class ReservationfinalDto {
	
	private String reservation_final_index;
	private String car_name;
	private String car_code;
	private String car_price;
	private String car_oil_type;
	private String car_option;
	private String max_car;
	private String place_x;
	private String place_y;
	private int reser_did;
	
	
	public String getReservation_final_index() {
		return reservation_final_index;
	}
	public void setReservation_final_index(String reservation_final_index) {
		this.reservation_final_index = reservation_final_index;
	}
	public String getCar_name() {
		return car_name;
	}
	public void setCar_name(String car_name) {
		this.car_name = car_name;
	}
	public String getCar_code() {
		return car_code;
	}
	public void setCar_code(String car_code) {
		this.car_code = car_code;
	}
	public String getCar_price() {
		return car_price;
	}
	public void setCar_price(String car_price) {
		this.car_price = car_price;
	}
	public String getCar_oil_type() {
		return car_oil_type;
	}
	public void setCar_oil_type(String car_oil_type) {
		this.car_oil_type = car_oil_type;
	}
	public String getCar_option() {
		return car_option;
	}
	public void setCar_option(String car_option) {
		this.car_option = car_option;
	}
	public String getMax_car() {
		return max_car;
	}
	public void setMax_car(String max_car) {
		this.max_car = max_car;
	}
	public String getPlace_x() {
		return place_x;
	}
	public void setPlace_x(String place_x) {
		this.place_x = place_x;
	}
	public String getPlace_y() {
		return place_y;
	}
	public void setPlace_y(String place_y) {
		this.place_y = place_y;
	}
	public int getReser_did() {
		return reser_did;
	}
	public void setReser_did(int reser_did) {
		this.reser_did = reser_did;
	}

	

}
