package com.javalec.spring_mybatis.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.javalec.spring_mybatis.dto.AreaDto;
import com.javalec.spring_mybatis.dto.ContentDto;
import com.javalec.spring_mybatis.dto.LicenceDto;
import com.javalec.spring_mybatis.dto.ReservationfinalDto;
import com.javalec.spring_mybatis.dto.User_MemberDto;
import com.javalec.spring_mybatis.dto.User_Reservation;
import com.javalec.spring_mybatis.dto.mtomListDto;
import com.javalec.spring_mybatis.dto.noticeCategoryDto;
import com.javalec.spring_mybatis.dto.noticeDto;
import com.javalec.spring_mybatis.dto.qnaCategoryDto;
import com.javalec.spring_mybatis.dto.qnaDto;

public interface IDao {
	
	public ArrayList<ContentDto> listDao();
	public void writeDao(String mWriter, String mContent);
	public ContentDto viewDao(String strID);
	public void deleteDao(String bId);
	public ArrayList<noticeDto> noticelistDao();
	public User_MemberDto checklogin(String id);
	public noticeDto noticeDetailDao(String no);
	public ArrayList<noticeCategoryDto> noticeCategoryDao();
	public void noticeWriteDao(int category, String subject, String contents);
	public ArrayList<qnaDto> qnalistDao();
	public ArrayList<mtomListDto> mtomListDao(String id);
	public mtomListDto mtomDetailDao();
	public int register(User_MemberDto mdto);
	public int licence(LicenceDto ldto);
	public int upregister(User_MemberDto mdto);
	public int uplicence(LicenceDto ldto);
	public List<AreaDto> listarea();
	public LicenceDto getLicence(String id);
	public ReservationfinalDto ReserDao(String positions_x, String positions_y);
	public ReservationfinalDto ReserDaoTEST();
	public ArrayList<ReservationfinalDto> ReserDaofinal(String positions_x, String positions_y);
	public void changereserdid(int reser_did);
	//public void insert_user_reserevation(String user_id,int reser_index,String min_time,String max_time,int price);
	public void insert_user_reserevation(User_Reservation udto);
	//#{user_id},#{reservation_final_index},#{min_time},#{max_time},#{price})
	public int noticelistCount();
	public noticeDto noticeDetailDao(int no);
	public ArrayList<qnaCategoryDto> qnaCategoryDao();
	public ArrayList<mtomListDto> mtomListDao2();
	public int mtomlistCount();
	public mtomListDto mtomDetailDao(int no);
	public void mtomWriteDao(String id, String category, String subject, String contents, String filename);
	
//changereserdid
}
