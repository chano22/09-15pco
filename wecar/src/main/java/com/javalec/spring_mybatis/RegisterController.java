package com.javalec.spring_mybatis;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.javalec.spring_mybatis.dao.IDao;
import com.javalec.spring_mybatis.dto.AreaDto;
import com.javalec.spring_mybatis.dto.LicenceDto;
import com.javalec.spring_mybatis.dto.User_MemberDto;

@Controller
public class RegisterController {

   @Autowired
   private SqlSession sqlSession;
   
   @Autowired
      private HttpSession session;
   
   @Autowired
	private SqlSessionTemplate sqlSessionTemplate;

   @RequestMapping(value = "/join1", method=RequestMethod.GET)
   public String userregisterview(){
      System.out.println("join1");
      return "jsp/login_join/join1";
   }
   
   public User_MemberDto person(HttpServletRequest req)throws Exception{
      System.out.println("person");
      User_MemberDto mdto=new User_MemberDto();
      mdto.setUsername(req.getParameter("name"));
      mdto.setId(req.getParameter("id"));
      mdto.setPw(req.getParameter("pw"));
      mdto.setPhone1(req.getParameter("phone1"));
      mdto.setPhone2(req.getParameter("phone2"));
      mdto.setPhone3(req.getParameter("phone3"));
      mdto.setEmail(req.getParameter("email"));
      mdto.setPOST(req.getParameter("post"));
      mdto.setAddr1(req.getParameter("addr1"));
      mdto.setAddr2(req.getParameter("addr2"));
      mdto.setArea(Integer.parseInt(req.getParameter("area")));
      
      //회원정보 확인 출력
      System.out.println("이름="+mdto.getUsername() + " /아이디="+mdto.getId()+
            " /이메일"+mdto.getEmail()+" /비밀번호="+mdto.getPw()+" /전화1="+
                  mdto.getPhone1()+" /전화22="+mdto.getPhone2()+" /전화3="+mdto.getPhone3()+
                  " /우편="+mdto.getPOST()+" /지역1="+mdto.getAddr1()+" /지역2="+mdto.getAddr2()+" /지역옵션="+mdto.getArea());
      
      return mdto;
   }
   
   public LicenceDto liperson(HttpServletRequest req)throws Exception{
      LicenceDto ldto = new LicenceDto();
      System.out.println("liperson");
      
      ldto.setId(req.getParameter("id"));
      ldto.setType(req.getParameter("carle"));
      ldto.setArea(Integer.parseInt(req.getParameter("area2")));
      ldto.setNum1(req.getParameter("num1"));
      ldto.setNum2(req.getParameter("num2"));
      ldto.setNum3(req.getParameter("num3"));
      ldto.setIssue_date(req.getParameter("issue_date"));
      ldto.setExp_date(req.getParameter("exp_date"));
      ldto.setBirthday(req.getParameter("birthday"));
      ldto.setGender(req.getParameter("gender"));
      
      System.out.println("id="+ldto.getId()+" /"+"type="+ldto.getType()+" /"+"지역="+ldto.getArea()+" /"
            +"num1="+ldto.getNum1()+" /"+"num2="+ldto.getNum2()+" /"+"num3="+ldto.getNum3()+" /"+
            "issue_date="+ldto.getIssue_date()+" /"+"exp_date="+ldto.getExp_date()+" /"+"birthday="+ldto.getBirthday()+" /"
            +"gender="+ldto.getGender());
      
      return ldto;
   }
   
   @RequestMapping(value="/join12")
   @Transactional
   public ModelAndView join1p(HttpServletRequest req, HttpServletResponse resp)throws Exception{
      System.out.println("join12");
      ModelAndView mav = new ModelAndView();
      IDao dao = sqlSession.getMapper(IDao.class);
      System.out.println("join122");
      int re = 0;
      int li = 0;
      String msg = null;
      String url = null;
      User_MemberDto mdto= person(req);
      LicenceDto ldto = liperson(req);
      
      System.out.println("join12에서 받아오는 mypage : " + req.getParameter("mypage"));
      if(req.getParameter("mypage").equals("mypage")){
         System.out.println("회원정보수정");
         
         re = dao.upregister(mdto);
         li = dao.uplicence(ldto);
         System.out.println("회원정보 입력결과 : "+re);
         System.out.println("라이센스 입력결과 : "+li);
         if(re>0 && li>0){//회원정보, 라이센스 입력성공
            msg = "수정되었습니다.";
            HttpSession session = req.getSession();
            session.setAttribute("dto", mdto);
         }else if(re<=0 && li>0){//회원정보 X, 라이센스 O
            msg = "회원정보 X, 라이센스 O";
         }else if(re>0 && li<=0){//회원정보 O, 라이센스 X
            msg = "회원정보 O, 라이센스 X";
         }
         url = "mypage";
      }else{
    	  System.out.println(req.getParameter("post"));
    	  System.out.println(req.getParameter("addr1"));
    	  System.out.println(req.getParameter("addr2"));
    	  
         System.out.println("가입");
         re = dao.register(mdto);
         li = dao.licence(ldto);
         System.out.println("회원정보 입력결과 : "+re);
         System.out.println("라이센스 입력결과 : "+li);
         if(re>0 && li>0){//회원정보, 라이센스 입력성공
            msg = "가입되었습니다.";
         }else if(re<=0 && li>0){//회원정보 X, 라이센스 O
            msg = "회원정보 X, 라이센스 O";
         }else if(re>0 && li<=0){//회원정보 O, 라이센스 X
            msg = "회원정보 O, 라이센스 X";
         }
         url = "";
      }

      mav.addObject("url", url);
      mav.addObject("msg",msg);
      mav.setViewName("message");
      return mav;
   }
   
//   @RequestMapping(value="mypage")
//   public ModelAndView mypage(HttpServletRequest req, HttpServletResponse resp)throws Exception{
//      System.out.println("mypage");
//      ModelAndView mav = new ModelAndView();
//      IDao dao = sqlSession.getMapper(IDao.class);
//      String msg = "null";
//      String url = "null";
//      
//      HttpSession session = req.getSession();
//      User_MemberDto mdto = new User_MemberDto();
//
//      System.out.println("mypage"+mdto.getId());
//      if(mdto.getId()==null || mdto.getId().equals("")){
//         System.out.println("로그인필요");
//         msg = "로그인 후 사용가능합니다.로그인페이지로 이동합니다.";
//         url ="login";
//         mav.addObject("msg", msg);
//         mav.addObject("url", url);
//         mav.setViewName("message");
//      }else{
//         System.out.println("로그인되어있음");
//         List<AreaDto> area = new ArrayList<AreaDto>();
//         area = dao.listarea();
//         mav.addObject("area", area);
//         LicenceDto ldto = dao.getLicence(mdto.getId());
//         mav.addObject("ldto", ldto);
//         mav.setViewName("jsp/mypage/mypage");   
//      }
//      
//      return mav;
//   }
   
   @ResponseBody
   @RequestMapping(value= "/LoginCheck",method=RequestMethod.POST)
   public int Logincheck(HttpServletRequest request, Model model)
   {
      System.out.println("LoginCheck");
      System.out.println("아이디체크메소드");
      String checkid=request.getParameter("userid");
      IDao dao = sqlSession.getMapper(IDao.class);
      System.out.println("넘어온값 : " + checkid);
      User_MemberDto mdto=new User_MemberDto();
         try{
            mdto=dao.checklogin(checkid);
            System.out.println(mdto.getId());
            if(mdto.getId().equals(checkid))
            {
               System.out.println("아이디가 이미있음");
               return 0;
            }      
         }
         catch (Exception e) {
            if(mdto==null)
            {
               System.out.println("아이디가 존재하지않음. 사용가능함.");
               return 1;
            }
         }         
      return 1;
   }
   
}