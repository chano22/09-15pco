package com.javalec.spring_mybatis;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.javalec.spring_mybatis.dao.IDao;
import com.javalec.spring_mybatis.dto.User_MemberDto;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	//ContentDao dao;
	
	@Autowired
	private SqlSession sqlSession;
/*	
	@Autowired
	public void setDao(ContentDao dao) {
		this.dao = dao;
	}*/
	public void showtime(Locale locale)
	{
		logger.info("GIT TEST4. 서버타임 : ");
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		System.out.println(formattedDate);
			
	}
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		System.out.println(formattedDate);
		model.addAttribute("serverTime", formattedDate );
		
		return "main";
	}
	
	   @RequestMapping(value="/login")
	   public String login(){
	      System.out.println("login");
	      return "jsp/login_join/login";
	   }
	   
	   @RequestMapping(value="login1")
	   public ModelAndView login1(HttpSession session, HttpServletRequest req, HttpServletResponse resp) throws Exception{
	      System.out.println("login1");
	      //
	      ModelAndView mav = new ModelAndView();
	      String id = req.getParameter("id");
	      String pw = req.getParameter("pw");
	      IDao dao = sqlSession.getMapper(IDao.class);
	      System.out.println("id : "+id+" /pw : "+pw);
	      
	    
	      User_MemberDto mdto = dao.checklogin(id);
	    //  System.out.println(id);
	     // System.out.println("id : "+mdto.getId()+" /pw : "+mdto.getPw()+" /name : "+mdto.getUsername());
	      
	      String msg = null;
	      String url = null;
	      if(id != null && mdto.getPw().equals(pw)){//아이디있고, 아이디비번맞고
	         System.out.println("아이디 있음  : "+id+"/"+ pw);
	         msg = "로그인되었습니다.";
	         url="";
	         session.setAttribute("dto", mdto);
//	         session.setAttribute("sessionid", id);
//	         session.setAttribute("sessionpw", pw);
	      }else if(mdto.getId() != null){//아이디 없음
	         System.out.println("아이디 없음 : "+id);
	         msg = "아이디가 없거나 비밀번호가 틀립니다.";
	         url = "login";
	      }
	      mav.addObject("url",url);
	      mav.addObject("msg",msg);
	      mav.setViewName("message");
	      return mav;
	            
	   }
	   
	   
	   @RequestMapping(value="logout")
	   public ModelAndView logout(HttpSession session, HttpServletRequest req, HttpServletResponse resp) throws Exception{
	      System.out.println("logout");
	      ModelAndView mav = new ModelAndView();
	      session.invalidate();
//	      User_MemberDto mdto = new User_MemberDto();
//	      session.removeAttribute("dto");
	      String msg = "로그아웃 되었습니다.";
	      String url = "";
	      mav.addObject("msg", msg);
	      mav.addObject("url", url);
	      mav.setViewName("message");
	      return mav;
	   }
}
