package com.javalec.spring_mybatis;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.core.Local;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.javalec.spring_mybatis.dao.IDao;
import com.javalec.spring_mybatis.dto.ReservationfinalDto;
import com.javalec.spring_mybatis.dto.User_MemberDto;
import com.javalec.spring_mybatis.dto.User_Reservation;

@Controller
public class ReserveController {
	
	
	@Autowired
	private SqlSession sqlSession;
	
	
	
	@ResponseBody
	@RequestMapping(value= "/getcars",method=RequestMethod.POST)
	public HashMap<String, Object> cardata(HttpServletRequest request, Model model)
	{
		System.out.println("데이터뜨냐??");	
		System.out.println("car를 가져온다.");
		String positions_x=request.getParameter("position_x");
		String positions_y=request.getParameter("position_y");
		
		IDao dao = sqlSession.getMapper(IDao.class);
		System.out.println("넘어온값 : " + positions_x);
		System.out.println("넘어온값 : " + positions_y);
		ArrayList<ReservationfinalDto> rdto2 = null;
		HashMap<String, Object> datas=new HashMap<String,Object>();
	
	try{
		rdto2=dao.ReserDaofinal(positions_x, positions_y);
		System.out.println(rdto2.get(0).getCar_name());
		System.out.println(rdto2.get(1).getCar_name());
			
			
		datas.put("list", rdto2);
	}
	catch(Exception e)
	{
		e.printStackTrace();
		if(rdto2==null)
		{
			System.out.println("좌표가 존재하지않음.");
			datas.put("error","you did error");
		}
	}
		/*
		User_MemberDto mdto=new User_MemberDto();
			try{
				mdto=dao.checklogin(checkid);
				if(mdto.getUser_id().equals(checkid))
				{
					System.out.println("아이디가 이미있음");
					return 0;
				}		
			}
			catch (Exception e) {
				if(mdto==null)
				{
					System.out.println("아이디가 존재하지않음. 사용가능함.");
					return 1;
				}
			}	
		*/
		datas.put("error","you did error");
		return 	datas;
	
	}
	
	@Transactional
	@ResponseBody
	@RequestMapping(value= "/getindex",method=RequestMethod.POST)
	public int getindex(HttpServletRequest request, Model model)
	{
		System.out.println("테스트");
		String user_id=(String) request.getParameter("userid");
		String reservation_final_index=(String) request.getParameter("reservation_index");
		String min_time=(String) request.getParameter("leftttime");
		String max_time=(String) request.getParameter("righttime");
		String price=(String) request.getParameter("price");
			
		
		System.out.println(price);
		System.out.println(user_id);
		System.out.println(reservation_final_index);
		System.out.println(min_time);
		System.out.println(max_time);
		ReservationfinalDto reser=new ReservationfinalDto();
		 IDao dao = sqlSession.getMapper(IDao.class);
		dao.changereserdid(Integer.parseInt(reservation_final_index));
	//    dao.insert_user_reserevation(user_id,Integer.parseInt(reservation_final_index),min_time,max_time,Integer.parseInt(price));
		
	    User_Reservation udto=new User_Reservation();
		
	    udto.setUser_id(user_id);
	    udto.setUser_reservation_index(Integer.parseInt(reservation_final_index));
	    udto.setMax_time(max_time);
	    udto.setMin_time(min_time);
	    udto.setPrice(Integer.parseInt(price));
	    dao.insert_user_reserevation(udto);
	    //int n=dao.insert_user_reserevation(udto);
		//public void insert_user_reserevation(String user_id,int reser_index,String min_time,String max_time,int price);
		
		
		HashMap<String, Object> datas=new HashMap<String,Object>();
		
		return 	0;
	}
	
	
	
	
}
