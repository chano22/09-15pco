<!-- 마이페이지 : 내 정보 -->

<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="mypagetop.jsp"%>
<%-- <%@include file="../../top.jsp"%> --%>

<style type="text/css">
                @import 'http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin';
                @import 'http://fonts.googleapis.com/css?family=Anton';

                body {
                    min-height: 500px;
                    background-color: #f3f3f3;
                    color: #333;
                    font: 13px/18px 'Open Sans', arial, sans-serif;
                    -webkit-font-smoothing: antialiased;
                }

                .navbar-nav > li > a {
                    padding-top: 15px;
                    padding-bottom: 15px;
                    line-height: 20px;
                }

                .top-bar {
                    background-color: #fff;
                    margin-bottom: 0px;
                    width: 100%;
                    height: 50px;
                    webkit-box-shadow: rgba(0, 0, 0, 0.14902) 0px 2px 4px 0px;
                    -moz-box-shadow: rgba(0, 0, 0, 0.14902) 0px 2px 4px 0px;
                    box-shadow: rgba(0, 0, 0, 0.14902) 0px 2px 4px 0px;
                }

                .menu-item {
                    line-height: 20px;
                    color: #3f729b;
                    font-weight: bold;
                    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
                    font-size: 13px;
                }

                .menu-item-flexible {
                    max-width: 230px;
                    text-overflow: ellipsis;
                    overflow: hidden;
                    white-space: nowrap;
                }

                .web-logo {
                    color: #3f729b;
                    font-size: 18px;
                    font-weight: 200;
                    line-height: 1;
                    padding-left: 0px;
                }

                @media (max-width: 768px) {
                    .web-logo {
                        padding-left: 15px;
                    }
                }

                .header-profile {
                    color: #3f729b;
                    font-weight: bold;
                    line-height: 1;
                    max-width: 100px;
                    text-overflow: ellipsis;
                    white-space: nowrap;
                    overflow: hidden;
                }

                .navbar-nav > li > a.header-profile {
                    padding-top: 10px;
                    padding-bottom: 10px;
                    line-height: 30px;
                }

                .user-name-box {
                    margin-left: 5px;
                    display: none;
                }

                @media (min-width:768px) {
                    .navbar-nav > li > a.header-profile {
                        padding-right: 5px;
                    }

                    .web-logo {
                        padding-left: 15px;
                    }
                }

                #bs-navbar {
                    background-color: #fff;
                }

                @media (max-width:767px) {
                    .navbar-nav > li > a.header-profile {
                        max-width: none;
                        text-overflow: inherit;
                        white-space: inherit;
                        overflow: inherit;
                        padding-top: 5px;
                        padding-bottom: 5px;
                        line-height: 25px;
                    }

                    .menu-item {
                        font-size: 14px;
                    }

                    .menu-item-flexible {
                        max-width: none;
                        text-overflow: inherit;
                        overflow: inherit;
                        white-space: inherit;
                    }

                    .user-name-box {
                        font-size: 14px;
                        display: inline-block;
                    }

                    .navbar-nav > li {
                        background-color: #eee;
                    }

                    #bs-navbar {
                        background-color: #eee;
                        font-size: 15px;
                        webkit-box-shadow: rgba(0, 0, 0, 0.14902) 0px 2px 4px 0px;
                        -moz-box-shadow: rgba(0, 0, 0, 0.14902) 0px 2px 4px 0px;
                        box-shadow: rgba(0, 0, 0, 0.14902) 0px 2px 4px 0px;
                    }
                }

                .navbar-toggle .icon-bar {
                    background-color: #3f729b;
                }

                .navbar-toggle {
                    padding-right: 0px;
                }

                @media (max-width: 768px) {
                    .top-bar {
                        padding-left: 0px;
                    }

                    .navbar-brand {
                    }
                }

                .container > .navbar-header,
                .container-fluid > .navbar-header,
                .container > .navbar-collapse,
                .container-fluid > .navbar-collapse {
                    margin-left: 0px;
                    margin-right: 0px;
                }

                /*
                    slide 스타일
                */


                .carousel .item img {
                    width: 100%;
                    height: 420px;
                }

                .carousel .nav {
                    text-align: center;
                    margin: 0 auto;
                    max-width: 1140px;
                }


                    .carousel .nav li img {
                        text-align: center;
                        margin: 0 auto;
                        padding: 15px 0 0 0;
                    }


                .carousel h2 {
                    text-transform: uppercase;
                    font-size: 18px;
                    text-align: center;
                }



                .carousel li.active {
                    background: #90d91b;
                }

                .nav-pills > li.active > a:focus, .nav-pills > li.active > a:hover {
                    color: #fff;
                    background: #90d91b;
                }


                .nav-pills a:hover {
                    z-index: 99;
                    background: #90d91b;
                    color: #fff;
                    position: relative;
                    cursor: pointer;
                }


                li:hover {
                    z-index: 99;
                    background: #90d91b;
                    color: #fff;
                    position: relative;
                    cursor: pointer;
                }


                li.active > a {
                    z-index: 99;
                    background: #90d91b;
                    color: #fff;
                    position: relative;
                    top: -20px;
                    cursor: pointer;
                }


                .nav-pills > li.active > a {
                    z-index: 99;
                    background: #90d91b;
                    color: #fff;
                    position: relative;
                    top: -20px;
                    cursor: pointer;
                }


                .carousel-control i {
                    position: relative;
                    top: 50%;
                    bottom: 0;
                    left: 0;
                    right: 0;
                    width: 100%;
                    font-size: 90px;
                    color: #90d91b;
                    text-align: center;
                    text-shadow: none;
                    filter: none;
                    opacity: 1;
                }


                .carousel-caption {
                    position: absolute;
                    top: 45%;
                    z-index: 10;
                    padding-top: 0px;
                    padding-bottom: 0px;
                    color: #fff;
                }


                    .carousel-caption h3 {
                        font-size: 88px;
                        right: 0;
                        top: 0;
                        left: 0;
                        z-index: 10;
                        color: #90d91b;
                        text-align: center;
                        text-shadow: 5px 5px 0px rgba(0,0,0,.9);
                    }

                .carousel {
                    height: 420px;
                    border-bottom: 10px solid #00d2ff;
                }

                .slide {
                    height: 420px;
                }

                .bottombody {
                    width: 500px;
                    height: 1140px;
                    border: 10px;
                    margin-left: 20%;
                    margin-right: 20%;
                }

                /*
             body적용


         */
                .hide-bullets {
                    list-style: none;
                    margin-left: -40px;
                    margin-top: 20px;
                }

                /*
                     footer
                 */
                .underfooter {
                    height: 300px;
                    width: 1920px;
                    background-color: dimgrey;
                }

                .footer-menu > li {
                    float: left;
                    list-style: none;
                    font-family: Dotum,tahaoma;
                    font-size: 12px;
                    line-height: 1.3em;
                    color: #666;
                }

                .slide {
                    margin-bottom: 30px;
                }

                .carousel-control {
                    opacity: 0;
                }

                    .carousel-control.left, .carousel-control.right {
                        background-image: none;
                    }

                .top-wrapper {
                    border-top: 4px solid #00d2ff;
                }

                    .top-wrapper > #MeuCarousel > .carousel-inner {
                        border-bottom: 4px solid #00d2ff;
                    }


                .col-sm-3:hover {
                    background: none;
                }

                .first-dark {
                    color: #e6e6e6;
                }

                .hr-dark {
                    border-bottom: 1px solid #e6e6e6;
                }

                .first-bright {
                    color: #00d2ff;
                }

                .hr-bright {
                    border-bottom: 1px solid #00d2ff;
                }

                .
                /*
               <div id="MeuCarousel" class="carousel slide" data-ride="carousel">


               <div id="select-first"><h5>SMART SOCAR</h5></div>
                                <hr id="select-hr"/>

        */

                /*
                    body


                */

                .stepwizard-step p {
    margin-top: 10px;
}

.stepwizard-row {
    display: table-row;
}

.stepwizard {
    display: table;
    width: 100%;
    position: relative;
}

.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}

.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;

}

.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}

.btn-circle {
  width: 30px;
  height: 30px;
  text-align: center;
  padding: 6px 0;
  font-size: 12px;
  line-height: 1.428571429;
  border-radius: 15px;
}

#checklogintext{
    
    display:inline;
    width:40%;
}
#checklogin{
    width:20%;
    
    vertical-align: unset;
}
.control-label{
    display:block;
}

#sel1,#li_first,#li_second,#li_third{
    display:inline;
    width:30%;
}
#birthday{
   width:25%;
   display:inline;
}
#phone1,#phone2,#phone3{
	width:20%;
	display:inline;
}
.hr-inline{
	display:inline;
	vertical-align: bottom;
}

    </style>
    <script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>


    <script type="text/javascript">
    
    function sample4_execDaumPostcode() {
        new daum.Postcode({
            oncomplete: function(data) {
                // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                // 도로명 주소의 노출 규칙에 따라 주소를 조합한다.
                // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
                var fullRoadAddr = data.roadAddress; // 도로명 주소 변수
                var extraRoadAddr = ''; // 도로명 조합형 주소 변수

                // 법정동명이 있을 경우 추가한다. (법정리는 제외)
                // 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
                if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
                    extraRoadAddr += data.bname;
                }
                // 건물명이 있고, 공동주택일 경우 추가한다.
                if(data.buildingName !== '' && data.apartment === 'Y'){
                   extraRoadAddr += (extraRoadAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                // 도로명, 지번 조합형 주소가 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
                if(extraRoadAddr !== ''){
                    extraRoadAddr = ' (' + extraRoadAddr + ')';
                }
                // 도로명, 지번 주소의 유무에 따라 해당 조합형 주소를 추가한다.
                if(fullRoadAddr !== ''){
                    fullRoadAddr += extraRoadAddr;
                }

                // 우편번호와 주소 정보를 해당 필드에 넣는다.
                document.getElementById('sample4_postcode').value = data.zonecode; //5자리 새우편번호 사용
                document.getElementById('sample4_roadAddress').value = fullRoadAddr;
                document.getElementById('sample4_jibunAddress').value = data.jibunAddress;

                // 사용자가 '선택 안함'을 클릭한 경우, 예상 주소라는 표시를 해준다.
                if(data.autoRoadAddress) {
                    //예상되는 도로명 주소에 조합형 주소를 추가한다.
                    var expRoadAddr = data.autoRoadAddress + extraRoadAddr;
                    document.getElementById('guide').innerHTML = '(예상 도로명 주소 : ' + expRoadAddr + ')';

                } else if(data.autoJibunAddress) {
                    var expJibunAddr = data.autoJibunAddress;
                    document.getElementById('guide').innerHTML = '(예상 지번 주소 : ' + expJibunAddr + ')';

                } else {
                    document.getElementById('guide').innerHTML = '';
                }
            }
        }).open();
    }

    
    function registerCheckFunction()
	{
		var userid=$('#checklogintext').val();
		console.log(userid);
		$.ajax({
			type: "POST",
			url: "LoginCheck",
			data: {"userid":userid},
			
			success: function(result){
				if(result=='1'){
					
					$('#checkMessage').html('사용가능합니다. ');
					$('#checkType').attr('class');					
				}
				else{
					$('#checkMessage').html('아이디가 이미 사용중입니다.');
					$('#checkType').attr('class');
				}
				$('#checkModal').modal("show");
			}
		})
	}
    
    
    jQuery(document).ready(function($){
    $('#MeuCarousel').carousel({
      interval:   4000
   });

   var clickEvent = false;
   $('#MeuCarousel').on('click', '.nav a', function() {
         clickEvent = true;
         $('.nav li').removeClass('active');
         $(this).parent().addClass('active');
   }).on('slid.bs.carousel', function(e) {
      if(!clickEvent) {
         var count = $('.nav').children().length -1;
         var current = $('.nav li.active');
         current.removeClass('active').next().addClass('active');
         var id = parseInt(current.data('slide-to'));
         if(count == id) {
            $('.nav li').first().addClass('active');
         }
      }
      clickEvent = false;
   });
  
   var navListItems = $('div.setup-panel div a'),
 allWells = $('.setup-content'),
 allNextBtn = $('.nextBtn');

   allWells.hide();

   navListItems.click(function (e) {
       e.preventDefault();
       var $target = $($(this).attr('href')),
               $item = $(this);

       if (!$item.hasClass('disabled')) {
           navListItems.removeClass('btn-primary').addClass('btn-default');
           $item.addClass('btn-primary');
           allWells.hide();
           $target.show();
           $target.find('input:eq(0)').focus();
       }
   });

   allNextBtn.click(function () {
       var curStep = $(this).closest(".setup-content"),
           curStepBtn = curStep.attr("id"),
           nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
           curInputs = curStep.find("input[type='text'],input[type='url']"),
           isValid = true;

       $(".form-group").removeClass("has-error");
       for (var i = 0; i < curInputs.length; i++) {
           if (!curInputs[i].validity.valid) {
               isValid = false;
               $(curInputs[i]).closest(".form-group").addClass("has-error");
           }
       }

       if (isValid)
           nextStepWizard.removeAttr('disabled').trigger('click');
   });

   $('div.setup-panel div a.btn-primary').trigger('click');

});
    </script>
    
        <div class="container">
        <div class="stepwizard">
            <div class="stepwizard-row setup-panel">
                <div class="stepwizard-step">
                    <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                    <p>회원정보 입력</p>
                </div>
<!--                 <div class="stepwizard-step"> -->
<!--                     <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a> -->
<!--                     <p>면허증 입력</p> -->
<!--                 </div> -->
            </div>
        </div>
        <form name="mypage2" action="join12" >
        <input name = "mypage" type="hidden" value = "mypage">
            <div class="row setup-content" id="step-1">
                <div class="col-xs-12">
                    <div class="col-md-12">
                        <h3> 회원정보</h3>
                        
                        <div class="form-group">
                            <label class="control-label">이름</label>
                            <input name="name" id="name" maxlength="100" type="text" required="required" class="form-control" value="${dto.username }" />
                        </div>
                        
                        <div class="form-group">
                            <input name="id" maxlength="100" type="hidden" id="checklogintext" required="required" class="form-control" value="${dto.id}"/>
                            <input name="pw" maxlength="100" type="hidden" required="required" class="form-control" value="${dto.pw }" />
                        </div>
                        
                         <div class="form-group">
                            <label class="control-label" id="phone">휴대폰 번호</label>
                            <input name="phone1" id="phone1" maxlength="100" type="text" class="form-control" value="${dto.phone1}"/>
                            <h1 class="hr-inline">-</h1>
                            <input name="phone2" id="phone2" maxlength="100" type="text" class="form-control" value="${dto.phone2}"/>
                            <h1 class="hr-inline">-</h1>
                            <input name="phone3" id="phone3" maxlength="100" type="text" class="form-control" value="${dto.phone3}"/>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label">이메일</label>
                            <input name="email" maxlength="100" type="text" class="form-control" value="${dto.email}"/>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label">주소</label>
                              <input type="text" name="post" id="sample4_postcode" class="form-control" value="${dto.post}">
							  <input type="button" onclick="sample4_execDaumPostcode()" class="form-control" value="우편번호 찾기"><br>
							  <input type="text" name="addr1" id="sample4_roadAddress" class="form-control" value="${dto.addr1 }">
							  <input type="text" name="addr2"id="sample4_jibunAddress" class="form-control" value="${dto.addr2 }">
<!-- 				      		<span id="guide" style="color:#999"></span> -->
                        </div>
                        
                        <div  class="form-group">
                        <c:forEach var="area" items="${area}">
                        	<c:choose>
                        		<c:when test="${dto.area==area.area_id}">
                        			<input type="radio" name = "area" value="${dto.area}" checked>${area.area_name}
                        		</c:when>
                        		<c:otherwise>
                        			<input type="radio" name="area" value="${area.area_id}">${area.area_name}
                        		</c:otherwise>
                        	</c:choose>
                        </c:forEach>

                        </div>
                        
                         <h3>운전면허</h3>
                        <div class="radio-inline">
                        <c:if test="${ldto.type=='1'}">
	                        <label class="radio-inline">
	                        <input type = "radio" name="carle" value = "1" checked>1종 보통
	                        </label>
	                        <label class="radio-inline">
	                        <input type = "radio" name="carle" value="2">2종 보통
	                        </label>
	                        <label class="radio-inline">
	                        <input type = "radio" name="carle" value="3">1종 대형
                       		 </label>
                        </c:if>
                        <c:if test="${ldto.type=='2'}">
	                        <label class="radio-inline">
	                        <input type = "radio" name="carle" value = "1" >1종 보통
	                        </label>
	                        <label class="radio-inline">
	                        <input type = "radio" name="carle" value="2" checked>2종 보통
	                        </label>
	                        <label class="radio-inline">
	                        <input type = "radio" name="carle" value="3">1종 대형
                       		 </label>
                        </c:if>
                        <c:if test="${ldto.type=='3'}">
	                        <label class="radio-inline">
	                        <input type = "radio" name="carle" value = "1" >1종 보통
	                        </label>
	                        <label class="radio-inline">
	                        <input type = "radio" name="carle" value="2" >2종 보통
	                        </label>
	                        <label class="radio-inline">
	                        <input type = "radio" name="carle" value="3" checked>1종 대형
                       		 </label>
                        </c:if>

                        </div>
                        <p>
                        <p>
                        <label for="sel1">면허번호</label>
                         <div class="form-group" id="li_num">
                         <select class="form-control" name="area2" id="sel1">
                           <c:forEach var="area" items="${area}">
                        	<c:choose>
                        		<c:when test="${ldto.area==area.area_id}">
                        		 <option value="${ldto.area}" selected>${area.area_name}</option>
                        		</c:when>
                        		<c:otherwise>
                        			<option value="${ldto.area}" >${area.area_name}</option>
                        		</c:otherwise>
                        	</c:choose>
                        </c:forEach>
                             
                             </select>
                             <input name="num1" id="li_first" maxlength="200" type="text" required="required" class="form-control" value="${ldto.num1 }" />
                             <input name="num2" id="li_first" maxlength="200" type="text" required="required" class="form-control" value="${ldto.num2}" />
                             <input name="num3" id="li_third" maxlength="200" type="text" required="required" class="form-control" value="${ldto.num3 }" />
                         </div>
                        
                        <div class="form-group">
                            <label class="control-label">면허 발급일</label>
                            <input type ="text" name="issue_date" id="datepicker" maxlength="100" required="required" class="form-control" value="${ldto.issue_date }" />
                        </div>
                        <div class="form-group">
                            <label class="control-label">갱신기간 만료일</label>
                            <input name="exp_date" id="datepicker" maxlength="100" type="text" required="required" class="form-control" value="${ldto.exp_date }" />
                        </div>
                        <div class="form-group">
                            <label class="control-label">생년월일/성별</label>
                            <input name="birthday"  id="datepicker" maxlength="100" id="birthday" type="text" required="required" class="form-control"value="${ldto.birthday }" />
                      		<c:choose>
                      			<c:when test="${ldto.gender=='0'}">
	                      			<label class="radio-inline">
	                                    <input type="radio" name="gender" value="0" checked>남자
	                                </label>
	                                <label class="radio-inline">
	                                    <input type="radio" name="gender" value="1">여자
	                                </label>
                      			</c:when>
                      			<c:otherwise>
	                      			<label class="radio-inline">
	                                    <input type="radio" name="gender" value="0" >남자
	                                </label>
	                                <label class="radio-inline">
	                                    <input type="radio" name="gender" value="1" checked>여자
	                                </label>
                      			</c:otherwise>
                      		</c:choose>
                      		
                        </div>
                        <div class="form-group">
                            <h6 style="color:red">· 주민등록 상에 기재된 생년월일을 입력해주세요.</h6>
                            <h6 style="color:red">· 만 21세 미만이거나 운전면허 취득 7년 미만일 경우 SO회원 승인이 불가합니다.</h6>
                            <h6 style="color:none">· 타인의 정보를 도용해 가입할 경우 법적 조치가 취해질 수 있으며, 유효한 정보가 아닐 경우 회원 승인이 거절될 수 있습니다.</h6>

                        </div>
<!--                         <button class="btn btn-primary nextBtn btn-lg pull-right" type="button">Next</button> -->
<!--                           <button class="btn btn-primary nextBtn btn-lg pull-right" type="button">Next</button> -->
                          <button class="btn btn-success btn-lg pull-right" type="submit">수정</button>
                    </div>
                </div>
            </div>
<!--             <div class="row setup-content" id="step-2"> -->
<!--                 <div class="col-xs-12"> -->
<!--                     <div class="col-md-12"> -->
<!--                         <h3>운전면허</h3> -->
<!--                         <div class="radio-inline"> -->
<%--                         <c:if test="${ldto.type=='1'}"> --%>
<!-- 	                        <label class="radio-inline"> -->
<!-- 	                        <input type = "radio" name="carle" value = "1" checked>1종 보통 -->
<!-- 	                        </label> -->
<!-- 	                        <label class="radio-inline"> -->
<!-- 	                        <input type = "radio" name="carle" value="2">2종 보통 -->
<!-- 	                        </label> -->
<!-- 	                        <label class="radio-inline"> -->
<!-- 	                        <input type = "radio" name="carle" value="3">1종 대형 -->
<!--                        		 </label> -->
<%--                         </c:if> --%>
<%--                         <c:if test="${ldto.type=='2'}"> --%>
<!-- 	                        <label class="radio-inline"> -->
<!-- 	                        <input type = "radio" name="carle" value = "1" >1종 보통 -->
<!-- 	                        </label> -->
<!-- 	                        <label class="radio-inline"> -->
<!-- 	                        <input type = "radio" name="carle" value="2" checked>2종 보통 -->
<!-- 	                        </label> -->
<!-- 	                        <label class="radio-inline"> -->
<!-- 	                        <input type = "radio" name="carle" value="3">1종 대형 -->
<!--                        		 </label> -->
<%--                         </c:if> --%>
<%--                         <c:if test="${ldto.type=='3'}"> --%>
<!-- 	                        <label class="radio-inline"> -->
<!-- 	                        <input type = "radio" name="carle" value = "1" >1종 보통 -->
<!-- 	                        </label> -->
<!-- 	                        <label class="radio-inline"> -->
<!-- 	                        <input type = "radio" name="carle" value="2" >2종 보통 -->
<!-- 	                        </label> -->
<!-- 	                        <label class="radio-inline"> -->
<!-- 	                        <input type = "radio" name="carle" value="3" checked>1종 대형 -->
<!--                        		 </label> -->
<%--                         </c:if> --%>

<!--                         </div> -->
<!--                         <p> -->
<!--                         <p> -->
<!--                         <label for="sel1">면허번호</label> -->
<!--                          <div class="form-group" id="li_num"> -->
<!--                          <select class="form-control" name="area2" id="sel1"> -->
<%--                            <c:forEach var="area" items="${area}"> --%>
<%--                         	<c:choose> --%>
<%--                         		<c:when test="${ldto.area==area.area_id}"> --%>
<%--                         		 <option value="${ldto.area}" selected>${area.area_name}</option> --%>
<%--                         		</c:when> --%>
<%--                         		<c:otherwise> --%>
<%--                         			<option value="${ldto.area}" >${area.area_name}</option> --%>
<%--                         		</c:otherwise> --%>
<%--                         	</c:choose> --%>
<%--                         </c:forEach> --%>
                             
<!--                              </select> -->
<%--                              <input name="num1" id="li_first" maxlength="200" type="text" required="required" class="form-control" value="${ldto.num1 }" /> --%>
<%--                              <input name="num2" id="li_first" maxlength="200" type="text" required="required" class="form-control" value="${ldto.num2}" /> --%>
<%--                              <input name="num3" id="li_third" maxlength="200" type="text" required="required" class="form-control" value="${ldto.num3 }" /> --%>
<!--                          </div> -->
                        
<!--                         <div class="form-group"> -->
<!--                             <label class="control-label">면허 발급일</label> -->
<%--                             <input type ="text" name="issue_date" id="datepicker" maxlength="100" required="required" class="form-control" value="${ldto.issue_date }" /> --%>
<!--                         </div> -->
<!--                         <div class="form-group"> -->
<!--                             <label class="control-label">갱신기간 만료일</label> -->
<%--                             <input name="exp_date" id="datepicker" maxlength="100" type="text" required="required" class="form-control" value="${ldto.exp_date }" /> --%>
<!--                         </div> -->
<!--                         <div class="form-group"> -->
<!--                             <label class="control-label">생년월일/성별</label> -->
<%--                             <input name="birthday"  id="datepicker" maxlength="100" id="birthday" type="text" required="required" class="form-control"value="${ldto.birthday }" /> --%>
<%--                       		<c:choose> --%>
<%--                       			<c:when test="${ldto.gender=='0'}"> --%>
<!-- 	                      			<label class="radio-inline"> -->
<!-- 	                                    <input type="radio" name="gender" value="0" checked>남자 -->
<!-- 	                                </label> -->
<!-- 	                                <label class="radio-inline"> -->
<!-- 	                                    <input type="radio" name="gender" value="1">여자 -->
<!-- 	                                </label> -->
<%--                       			</c:when> --%>
<%--                       			<c:otherwise> --%>
<!-- 	                      			<label class="radio-inline"> -->
<!-- 	                                    <input type="radio" name="gender" value="0" >남자 -->
<!-- 	                                </label> -->
<!-- 	                                <label class="radio-inline"> -->
<!-- 	                                    <input type="radio" name="gender" value="1" checked>여자 -->
<!-- 	                                </label> -->
<%--                       			</c:otherwise> --%>
<%--                       		</c:choose> --%>
                      		
<!--                         </div> -->
<!--                         <div class="form-group"> -->
<!--                             <h6 style="color:red">· 주민등록 상에 기재된 생년월일을 입력해주세요.</h6> -->
<!--                             <h6 style="color:red">· 만 21세 미만이거나 운전면허 취득 7년 미만일 경우 SO회원 승인이 불가합니다.</h6> -->
<!--                             <h6 style="color:none">· 타인의 정보를 도용해 가입할 경우 법적 조치가 취해질 수 있으며, 유효한 정보가 아닐 경우 회원 승인이 거절될 수 있습니다.</h6> -->

<!--                         </div> -->
<!--                         <button class="btn btn-primary nextBtn btn-lg pull-right" type="button">Next</button> -->
<!--                     </div> -->
<!--                 </div> -->
<!--             </div> -->

        </form>
    </div>
    
    
    

<%@include file="../../bottom.jsp"%>