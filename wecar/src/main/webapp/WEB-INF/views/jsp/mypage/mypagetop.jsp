<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="../../top.jsp"%>

<style type="text/css">
@import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);
body{margin-top:20px;}
.fa-fw {width: 2em;}
</style>

<script type="text/javascript">
$(document).ready(function()
		{
		    var navItems = $('.admin-menu li > a');
		    var navListItems = $('.admin-menu li');
		    var allWells = $('.admin-content');
		    var allWellsExceptFirst = $('.admin-content:not(:first)');
		    
		    allWellsExceptFirst.hide();
		    navItems.click(function(e)
		    {
		        e.preventDefault();
		        navListItems.removeClass('active');
		        $(this).closest('li').addClass('active');
		        
		        allWells.hide();
		        var target = $(this).attr('data-target-id');
		        $('#' + target).show();
		    });
		});
</script>


<div class="container">
    <div class="row">
        <div class="col-md-3">
            <ul class="nav nav-pills nav-stacked admin-menu">
                <li class="active"><a href="mypage" data-target-id="mypage"><i class="fa fa-home fa-fw"></i>마이페이지</a></li>
                <li><a href="http://www.jquery2dotnet.com" data-target-id="widgets"><i class="fa fa-list-alt fa-fw"></i>예약내역</a></li>
                <li><a href="http://www.jquery2dotnet.com" data-target-id="pages"><i class="fa fa-file-o fa-fw"></i>내 쿠폰</a></li>
                <li><a href="http://www.jquery2dotnet.com" data-target-id="charts"><i class="fa fa-bar-chart-o fa-fw"></i>결제내역</a></li>
<!--                 <li><a href="http://www.jquery2dotnet.com" data-target-id="table"><i class="fa fa-table fa-fw"></i>Table</a></li> -->
<!--                 <li><a href="http://www.jquery2dotnet.com" data-target-id="forms"><i class="fa fa-tasks fa-fw"></i>Forms</a></li> -->
<!--                 <li><a href="http://www.jquery2dotnet.com" data-target-id="calender"><i class="fa fa-calendar fa-fw"></i>Calender</a></li> -->
<!--                 <li><a href="http://www.jquery2dotnet.com" data-target-id="library"><i class="fa fa-book fa-fw"></i>Library</a></li> -->
<!--                 <li><a href="http://www.jquery2dotnet.com" data-target-id="applications"><i class="fa fa-pencil fa-fw"></i>Applications</a></li> -->
<!--                 <li><a href="http://www.jquery2dotnet.com" data-target-id="settings"><i class="fa fa-cogs fa-fw"></i>Settings</a></li> -->
            </ul>
        </div>
        <div class="col-md-9 well admin-content" id="mypage">
            <p>
                Hello! This is a forked snippet.<br>
                It is for users, which use one-page layouts.
            </p>
            <p>
                Here's the original one from BhaumikPatel: <a href="http://bootsnipp.com/snippets/featured/vertical-admin-menu" target="_BLANK">Vertical Admin Menu</a>
                <br>
                Thank you Baumik!
            </p>
        </div>
        <div class="col-md-9 well admin-content" id="widgets">
            Widgets
        </div>
        <div class="col-md-9 well admin-content" id="pages">
            Pages
        </div>
        <div class="col-md-9 well admin-content" id="charts">
            Charts
        </div>
<!--         <div class="col-md-9 well admin-content" id="table"> -->
<!--             Table -->
<!--         </div> -->
<!--         <div class="col-md-9 well admin-content" id="forms"> -->
<!--             Forms -->
<!--         </div> -->
<!--         <div class="col-md-9 well admin-content" id="calender"> -->
<!--             Calender -->
<!--         </div> -->
<!--         <div class="col-md-9 well admin-content" id="library"> -->
<!--             Library -->
<!--         </div> -->
<!--         <div class="col-md-9 well admin-content" id="applications"> -->
<!--             Applications -->
<!--         </div> -->
<!--         <div class="col-md-9 well admin-content" id="settings"> -->
<!--             Settings -->
<!--         </div> -->
    </div>
</div>            

<div class="col-md-9 col-lg-9">
</div>