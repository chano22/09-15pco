<!-- 고객센터 : 1:1상담 글 보기 -->


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%@ page import="com.javalec.spring_mybatis.dto.mtomListDto"%>
<%@ page import="com.javalec.spring_mybatis.dto.qnaCategoryDto"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="../../top.jsp"%>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>1:1 상담</title>
<link
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css"
	rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Pacifico'
	rel='stylesheet' type='text/css'>
<link
	href="https://netdna.bootstrapcdn.com/font-awesome/4.0.0/css/font-awesome.css"
	rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Ubuntu'
	rel='stylesheet' type='text/css'>
<style>
@import
	url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css)
	;

body {
	margin-top: 20px;
}

a.menu {
	color: black;
}

a.contents {
	color: black;
}

.nav-pills>li.active>a {
	background: #428bca;
	top: 0px;
}

li.active>a {
	top: 0px;
}

.nav-pills a:hover {
	color: black;
}

.fa-fw {
	width: 2em;
}

.ubuntu {
	font-family: 'Ubuntu', sans-serif;
}

p {
	font-family: 'Ubuntu', sans-serif;
	text-align: justify;
}

.pacifico {
	font-family: 'Pacifico', sans-serif;
	font-size: 2.4em;
}

.header {
	padding: 10px;
}

.post h2 {
	font-size: 30px;
	font-weight: bold;
	line-height: 1.2;
}

.post h2 a {
	color: #333;
	text-decoration: none;
	display: block;
}

h2 {
	font-family: 'Helvetica Neue', Arial, Helvetica, sans-serif,
		"Helvetica Neue", HelveticaNeue, Arial, sans-serif;
}

.post {
	border-radius: 4px;
	overflow: hidden;
	padding: 0;
	position: relative;
	-webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.20);
	-moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.20);
	box-shadow: 0 1px 2px rgba(0, 0, 0, 0.20);
	background-clip: padding-box;
	background: #fff;
}

.things {
	opacity: 0.3;
}

.panel:hover .things {
	display: block;
	opacity: 1;
	transition: width, border opacity .0s linear .2s;
	-webkit-transition: width, border opacity .0s linear .2s;
	-moz-transition: width, border opacity .0s linear .2s;
	-ms-transition: width, border opacity .0s linear .2s;
	transition: opacity .2s linear .2s;
	-webkit-transition: opacity .2s linear .2s;
	-moz-transition: opacity .2s linear .2s;
	-ms-transition: opacity .2s linear .2s;
}

.panel-footer {
	background-color: #ffffff !important;
	font-size: 16px;
}

.panel-title {
	font-size: 20px;
	height: 25px;
}

h3 small, .h3 small, h3 .small, .h3 .small {
	font-size: 80%;
}

.panel {
	position:center;
}

.panel>.panel-heading:after{
	border-width:7px;
	border-right-color:#f7f7f7;
	margin-top:1px;
	margin-left:2px;
}

.panel>.panel-heading:before{
	border-right-color:#ddd;
	border-width:8px;
}

</style>
</head>
<body>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script
		src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<ul class="nav nav-pills nav-stacked admin-menu">
					<li><a class="menu" href="notice" data-target-id="notice"><i
							class="fa fa-home fa-fw"></i>공지사항</a></li>
					<li><a class="menu" href="question" data-target-id="question"><i
							class="fa fa-list-alt fa-fw"></i>Q&A</a></li>
					<li class="active"><a class="menu" href="mantoman"
						data-target-id="mantoman"><i class="fa fa-file-o fa-fw"></i>1:1
							문의</a></li>
				</ul>
			</div>
			<div class="col-md-9 no-gutter">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title" align="center">
							[${requestScope.mtomDetailDto.category}]
							&nbsp;${requestScope.mtomDetailDto.subject}
							<div class="small pull-right text-right">
								${requestScope.mtomDetailDto.writeday}</div>
						</h3>
					</div>
					<div class="panel-body">${requestScope.mtomDetailDto.contents}
						<br> <br> <br> <br> <br>
						<div class="row">
						 	<div class="col-md-12">
      							<h3 class="page-header">Comments</h2>
      						</div>
      					</div>
							<div class="row">
								<div class="col-md-1">
								</div>
								<div class="col-md-10">
									<strong>myusername</strong> <span class="text-muted">commented
												5 days ago</span>
									<div class="panel panel-default">
										<div class="panel-body">Panel content</div>
									</div> 		
								</div>	
								<div class="col-md-1">
								</div>	
								</div>
								<div id="result" ></div>			

						<div class="form-group">
							<div class="col-md-10">
								<textarea class="form-control" id="textarea" name="textarea"
									cols="30" rows="3" placeholder="댓글을 입력하세요"></textarea>
							</div>
							<div class="col-md-2 pull-right">
								<br>
								<a id="singlebutton" name="singlebutton"
									class="btn btn-default" href="javascript:registComm()">등록</a>
							</div>
							<input id="id" name="id" type="hidden" value="${dto.id}">
						</div>
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="col-md" align="center">
						<a class="btn btn-default" href="mantoman">목록보기</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<%@include file="../../bottom.jsp"%>
<script>
function registComm(){
    $.ajax({
        type: "post",
        url: "comment",
        success: function(){
        	var id = document.getElementById("id").value;
        	var contents = document.getElementById("textarea").value;
        	console.log('Current ID : ' + id + ' / contents : ' + contents);
           	var comment = '';
           	comment += '<div class="row">';
           	comment += '<div class="col-md-1"></div>';
           	comment += '<div class="col-md-10">';
           	comment += '<strong>';
           	comment += id;
           	comment += '</strong> <span class="text-muted">commented 5 days ago</span>';
           	comment += '<div class="panel panel-default">';
           	comment += '<div class="panel-body">';
           	comment += contents;
           	comment += '</div></div></div>';
           	comment += '<div class="col-md-1"></div></div>';
           
           $("#result").append(comment);
           $("#textarea").empty();
           console.log(contents);
        }
    });
}
</script>
</body>
</html>
