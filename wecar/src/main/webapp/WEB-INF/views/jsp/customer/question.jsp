<!-- 고객센터 : 공지사항 -->


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%@ page import="com.javalec.spring_mybatis.dto.qnaDto"%>
<%@ page import="com.javalec.spring_mybatis.dto.qnaCategoryDto"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="../../top.jsp"%>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Q&A</title>
<link
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css"
	rel="stylesheet">
<script>
	$(document).ready(function() {
		$('[id^=detail-]').hide();
		$('.toggle').click(function() {
			$input = $(this);
			$target = $('#' + $input.attr('data-toggle'));
			$target.slideToggle();
		});
	});
</script>
<style>
@import
	url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css)
	;


a.menu {
	color: black;
}

a.contents {
	color: black;
}

.nav-pills a:hover {
	color: black;
}

.nav-pills>li.active>a {
	background: #428bca;
	top: 0px;
}

li:hover {
	background: #428bca;
}

li.active>a {
	top: 0px;
}

.fa-fw {
	width: 2em;
}

.panel.with-nav-tabs .panel-heading {
	padding: 5px 5px 0 5px;
}

.panel.with-nav-tabs .nav-tabs {
	border-bottom: none;
}

.panel.with-nav-tabs .nav-justified {
	margin-bottom: -1px;
}

/********************************************************************/
/*** PANEL DEFAULT ***/
.with-nav-tabs.panel-default .nav-tabs>li>a, .with-nav-tabs.panel-default .nav-tabs>li>a:hover,
	.with-nav-tabs.panel-default .nav-tabs>li>a:focus {
	color: #777;
}

.with-nav-tabs.panel-default .nav-tabs>.open>a, .with-nav-tabs.panel-default .nav-tabs>.open>a:hover,
	.with-nav-tabs.panel-default .nav-tabs>.open>a:focus, .with-nav-tabs.panel-default .nav-tabs>li>a:hover,
	.with-nav-tabs.panel-default .nav-tabs>li>a:focus {
	color: #777;
	background-color: #ddd;
	border-color: transparent;
}

.with-nav-tabs.panel-default .nav-tabs>li.active>a, .with-nav-tabs.panel-default .nav-tabs>li.active>a:hover,
	.with-nav-tabs.panel-default .nav-tabs>li.active>a:focus {
	color: #555;
	background-color: #fff;
	border-color: #ddd;
	border-bottom-color: transparent;
}

.with-nav-tabs.panel-default .nav-tabs>li.dropdown .dropdown-menu {
	background-color: #f5f5f5;
	border-color: #ddd;
}

.with-nav-tabs.panel-default .nav-tabs>li.dropdown .dropdown-menu>li>a {
	color: #777;
}

.with-nav-tabs.panel-default .nav-tabs>li.dropdown .dropdown-menu>li>a:hover,
	.with-nav-tabs.panel-default .nav-tabs>li.dropdown .dropdown-menu>li>a:focus
	{
	background-color: #ddd;
}

.with-nav-tabs.panel-default .nav-tabs>li.dropdown .dropdown-menu>.active>a,
	.with-nav-tabs.panel-default .nav-tabs>li.dropdown .dropdown-menu>.active>a:hover,
	.with-nav-tabs.panel-default .nav-tabs>li.dropdown .dropdown-menu>.active>a:focus
	{
	color: #fff;
	background-color: #555;
}
/********************************************************************/
/*** PANEL PRIMARY ***/
.with-nav-tabs.panel-primary .nav-tabs>li>a, .with-nav-tabs.panel-primary .nav-tabs>li>a:hover,
	.with-nav-tabs.panel-primary .nav-tabs>li>a:focus {
	color: #fff;
}

.with-nav-tabs.panel-primary .nav-tabs>.open>a, .with-nav-tabs.panel-primary .nav-tabs>.open>a:hover,
	.with-nav-tabs.panel-primary .nav-tabs>.open>a:focus, .with-nav-tabs.panel-primary .nav-tabs>li>a:hover,
	.with-nav-tabs.panel-primary .nav-tabs>li>a:focus {
	color: #fff;
	background-color: #3071a9;
	border-color: transparent;
}

.with-nav-tabs.panel-primary .nav-tabs>li.active>a, .with-nav-tabs.panel-primary .nav-tabs>li.active>a:hover,
	.with-nav-tabs.panel-primary .nav-tabs>li.active>a:focus {
	color: #428bca;
	background-color: #fff;
	border-color: #428bca;
	border-bottom-color: transparent;
	top: 0px;
}

.with-nav-tabs.panel-primary .nav-tabs>li.dropdown .dropdown-menu {
	background-color: #428bca;
	border-color: #3071a9;
}

</style>

</head>
<body>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script
		src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<ul class="nav nav-pills nav-stacked admin-menu">
					<li>
						<a class="menu" href="notice" data-target-id="notice">
						<i class="fa fa-home fa-fw"></i>공지사항</a>
					</li>
					<li class="active">
						<a class="menu" href="question" data-target-id="question">
						<i class="fa fa-list-alt fa-fw"></i>Q&A</a>
					</li>
					<li>
						<a class="menu" href="mantoman" data-target-id="mantoman">
						<i class="fa fa-file-o fa-fw"></i>1:1 문의</a>
					</li>
				</ul>
			</div>
			<div class="col-md-9 well admin-content pull-right">
				<div class="row">
					<div class="col-md-12">
						<div class="panel with-nav-tabs panel-primary">
							<div class="panel-body">
								<div class="tab-content">
									<div class="tab-pane fade in active"
										id="${pageScope.list.name}">
										<div class="panel panel-default">
											<ul class="list-group">
												<li class="list-group-item">
													<div class="row toggle" id="dropdown-detail-1"
														data-toggle="detail-1">
														<div class="col-xs-10">Item 1</div>
														<div class="col-xs-2">
															<i class="fa fa-chevron-down pull-right"></i>
														</div>
													</div>
													<div id="detail-1">
														<hr></hr>
														<div class="container">
															<div class="fluid-row">
																<div class="col-xs-12">Detail</div>
															</div>
														</div>
													</div>
												</li>
												<li class="list-group-item">
													<div class="row toggle" id="dropdown-detail-2"
														data-toggle="detail-2">
														<div class="col-xs-10">Item 2</div>
														<div class="col-xs-2">
															<i class="fa fa-chevron-down pull-right"></i>
														</div>
													</div>
													<div id="detail-2">
														<hr></hr>
														<div class="container">
															<div class="fluid-row">
																<div class="col-xs-12">Detail:</div>
															</div>
														</div>
													</div>
												</li>
												<li class="list-group-item">
													<div class="row toggle" id="dropdown-detail-3"
														data-toggle="detail-3">
														<div class="col-xs-10">Item 3</div>
														<div class="col-xs-2">
															<i class="fa fa-chevron-down pull-right"></i>
														</div>
													</div>
													<div id="detail-3">
														<hr></hr>
														<div class="container">
															<div class="fluid-row">
																<div class="col-xs-12">Detail:</div>
															</div>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<br />
				<c:choose>
					<c:when test="${requestScope.CurrentID == 'admin'}">
						<a class="btn btn-default pull-right" href="questionWrite">글쓰기</a>
						<!--  관리자인 경우만 글쓰기 버튼 활성화 -->
					</c:when>
				</c:choose>
			</div>
		</div>

	</div>
	<script type="text/javascript">
				$(document).ready(function() {
					$('#1').on('click', function() {
					console.log("1");
					});
					$('#2').on('click', function() {
						console.log("2");
					});
					$('#3').on('click', function() {
						console.log("3");
					});
					$('#4').on('click', function() {
						console.log("4");
					});
					$('#5').on('click', function() {
						console.log("5");
					});
					$('#6').on('click', function() {
						console.log("6");
					});
				});										
	</script>
									
	<%@include file="../../bottom.jsp"%>
</body>
</html>
