<!-- 고객센터 : 자주하는질문 -->


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="../../top.jsp"%>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Q&A</title>
<link
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css"
	rel="stylesheet">

<style>
@import
	url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css)
	;


.fa-fw {
	width: 2em;
}

a.menu {
	color: black;
}

a.contents {
	color: black;
}

.nav-pills a:hover {
	color : black;
}

.nav-pills>li.active>a {
	background: #428bca;
	top: 0px;
}

</style>

</head>
<body>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script
		src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
	<div class="container">

		<div class="row">
			<div class="col-md-3">
					<ul class="nav nav-pills nav-stacked admin-menu">
						<li><a class="menu" href="notice" data-target-id="notice"><i
								class="fa fa-home fa-fw"></i>공지사항</a></li>
						<li class="active"><a class="menu" href="question" data-target-id="question"><i
								class="fa fa-list-alt fa-fw"></i>Q&A</a></li>
						<li><a class="menu" href="mantoman" data-target-id="mantoman"><i
								class="fa fa-file-o fa-fw"></i>1:1 문의</a></li>
					</ul>
				</div>
			<div class="container">
				<div class="row">
					<div class="col-md-9 ">
						<div class="well well-sm">
							<form class="form-horizontal" action="${pageContext.request.contextPath}/questionWriteOK" method="get">
								<fieldset>
									<legend class="text-center">Contact us</legend>

									<!-- Name input-->
									<div class="form-group">
										<label class="col-md-2 control-label" for="name">Name</label>
										<div class="col-md-9">
											<input id="name" name="name" type="text"
												placeholder="Your name" class="form-control">
										</div>
									</div>

									<!-- Email input-->
									<div class="form-group">
										<label class="col-md-2 control-label" for="email">Your
											E-mail</label>
										<div class="col-md-9">
											<input id="email" name="email" type="text"
												placeholder="Your email" class="form-control">
										</div>
									</div>

									<!-- Message body -->
									<div class="form-group">
										<label class="col-md-2 control-label" for="message">Your
											message</label>
										<div class="col-md-9">
											<textarea class="form-control" id="message" name="message"
												placeholder="Please enter your message here..." rows="5"></textarea>
										</div>
									</div>

									<!-- Form actions -->
									<div class="form-group">
										<div class="col-md-3 pull-right">
											<button type="submit" class="btn btn-primary btn-lg" >글쓰기</button>
										</div>
									</div>
								</fieldset>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

	<%@include file="../../bottom.jsp"%>
</body>
</html>