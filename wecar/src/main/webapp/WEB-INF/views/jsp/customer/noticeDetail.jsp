<!-- 고객센터 : 공지사항 글 보기 -->


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%@ page import="com.javalec.spring_mybatis.dto.noticeDto"%>
<%@ page import="com.javalec.spring_mybatis.dto.noticeCategoryDto"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="../../top.jsp"%>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>공지사항</title>
<link
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css"
	rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Pacifico'
	rel='stylesheet' type='text/css'>
<link
	href="https://netdna.bootstrapcdn.com/font-awesome/4.0.0/css/font-awesome.css"
	rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Ubuntu'
	rel='stylesheet' type='text/css'>
<style>
@import
	url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css)
	;


.fa-fw {
	width: 2em;
}

a.menu {
	color: black;
}

a.contents {
	color: black;
}

.nav-pills a:hover {
	color: black;
}

.nav-pills>li.active>a {
	background: #428bca;
	top: 0px;
}

body {
	margin-top: 20px;
}

.ubuntu {
	font-family: 'Ubuntu', sans-serif;
}

p {
	font-family: 'Ubuntu', sans-serif;
	text-align: justify;
}

.pacifico {
	font-family: 'Pacifico', sans-serif;
	font-size: 2.4em;
}

.header {
	padding: 10px;
}

.post h2 {
	font-size: 30px;
	font-weight: bold;
	line-height: 1.2;
}

.post h2 a {
	color: #333;
	text-decoration: none;
	display: block;
}

h2 {
	font-family: 'Helvetica Neue', Arial, Helvetica, sans-serif,
		"Helvetica Neue", HelveticaNeue, Arial, sans-serif;
}

.post {
	border-radius: 4px;
	overflow: hidden;
	padding: 0;
	position: relative;
	-webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.20);
	-moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.20);
	box-shadow: 0 1px 2px rgba(0, 0, 0, 0.20);
	background-clip: padding-box;
	background: #fff;
}

.things {
	opacity: 0.3;
}

.panel:hover .things {
	display: block;
	opacity: 1;
	transition: width, border opacity .0s linear .2s;
	-webkit-transition: width, border opacity .0s linear .2s;
	-moz-transition: width, border opacity .0s linear .2s;
	-ms-transition: width, border opacity .0s linear .2s;
	transition: opacity .2s linear .2s;
	-webkit-transition: opacity .2s linear .2s;
	-moz-transition: opacity .2s linear .2s;
	-ms-transition: opacity .2s linear .2s;
}

.panel-footer {
	background-color: #ffffff !important;
	font-size: 16px;
}

.panel-title {
	font-size:20px;
	height: 25px;
}

h3 small, .h3 small, h3 .small, .h3 .small {
	font-size:80%;
}
</style>

</head>
<body>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script
		src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<ul class="nav nav-pills nav-stacked admin-menu">
					<li class="active"><a class="menu" href="notice"
						data-target-id="notice"><i class="fa fa-home fa-fw"></i>공지사항</a></li>
					<li><a class="menu" href="question" data-target-id="question"><i
							class="fa fa-list-alt fa-fw"></i>Q&A</a></li>
					<li><a class="menu" href="mantoman" data-target-id="mantoman"><i
							class="fa fa-file-o fa-fw"></i>1:1 문의</a></li>
				</ul>
			</div>
			<div class="col-md-9 no-gutter">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title" align="center">
									[${requestScope.noticeDetailDto.name}]
									&nbsp;${requestScope.noticeDetailDto.subject}
								<div class="small pull-right text-right">
									<c:set var="writeday"
										value="${requestScope.noticeDetailDto.writeday}" />
									${fn:substring(writeday, 0, 10)}
								</div>
						</h3>
					</div>
					<div class="panel-body">${requestScope.noticeDetailDto.contents}</div>
				</div>
				<br>
				<div class="form-group">
					<div class="col-md" align="center">
						<a class="btn btn-default" href="notice">목록보기</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<%@include file="../../bottom.jsp"%>
</body>
</html>
