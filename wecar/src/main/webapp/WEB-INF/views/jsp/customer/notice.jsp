<!-- 고객센터 : 공지사항 -->

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%@ page import="com.javalec.spring_mybatis.dto.noticeDto"%>
<%@ page import="com.javalec.spring_mybatis.dto.noticeCategoryDto"%>
<%@ page import="com.javalec.spring_mybatis.dto.Paging"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix= "fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="../../top.jsp"%>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>공지사항</title>
<link
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css"
	rel="stylesheet">

<script type="text/javascript">

</script>

<style>
@import
	url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css)
	;

.fa-fw {
	width: 2em;
}

a.menu {
	color: black;
}

a.contents {
	color: black;
}

.nav-pills>li.active>a {
	background: #428bca;
	top: 0px;
}

li.active>a {
	top: 0px;
}

.nav-pills a:hover {
	color : black;
}

.btn {
	padding: 9px 13px;
}

#custom-search-form {
	margin: 0;
	margin-top: 5px;
	padding: 0;
}

#custom-search-form .search-query {
	padding-right: 3px;
	padding-right: 4px \9;
	padding-left: 3px;
	padding-left: 4px \9;
	/* IE7-8 doesn't have border-radius, so don't indent the padding */
	margin-bottom: 0;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
}

#custom-search-form button {
	border: 0;
	background: none;
	/** belows styles are working good */
	padding: 2px 5px;
	margin-top: 2px;
	position: relative;
	left: -28px;
	/* IE7-8 doesn't have border-radius, so don't indent the padding */
	margin-bottom: 0;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
}

.search-query:focus+button {
	z-index: 3;
}

.input-group-btn:last-child>.btn, .input-group-btn:last-child>.btn-group {
	margin-left: -1px; 
    margin-bottom: 10px;
}

</style>

</head>
<body>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script
		src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
	<form>
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<ul class="nav nav-pills nav-stacked admin-menu">
						<li class="active"><a class="menu" href="#" data-target-id="notice"><i
								class="fa fa-home fa-fw"></i>공지사항</a></li>
						<li><a class="menu" href="question" data-target-id="question"><i
								class="fa fa-list-alt fa-fw"></i>Q&A</a></li>
						<li><a class="menu" href="mantoman" data-target-id="mantoman"><i
								class="fa fa-file-o fa-fw"></i>1:1 문의</a></li>
					</ul>
				</div>
				<div class="col-md-9 well admin-content">
					<div class="row">
						<div id="custom-search-input">
							<div class="input-group col-md-4 pull-right">
								<input type="text" class="  search-query form-control" /> <span
									class="input-group-btn">
									<button class="btn btn-default" type="button">
										<span class=" glyphicon glyphicon-search"></span>
									</button>
								</span>
								&nbsp; &nbsp;
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<table class="table table-hover table-list-search">
								<thead>
									<tr>
										<th width=20>NO</th>
										<th>제목</th>
										<th width=150>작성일</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="list" items="${requestScope.noticeDto}"
										varStatus="loop">
										<c:url var="url" value="noticeDetail">
											<c:param name="no" value="${pageScope.list.no}" />
										</c:url>
										<tr>
											<td align="center">${list.no}</td>
											<td><a class="contents" href="${pageScope.url}">[${pageScope.list.name}]
													${pageScope.list.subject}</a></td>
											<c:set var="writeday" value="${list.writeday}" />
											<td>${fn:substring(writeday, 0, 10)}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4"></div>
						<div class="col-md-8">
							<ul class="pagination">
								<li><a href="#">&laquo;</a></li>
								<c:forEach var="i" begin="${requestScope.paging.startPageNo}"
									end="${requestScope.paging.endPageNo}" step="1">
									<li><a href="#">${i}</a></li>
								</c:forEach>
								<li><a href="#">&raquo;</a></li>
							</ul>
							<c:choose>
								<c:when test="${requestScope.CurrentID == 'admin'}">
									<a class="btn btn-default pull-right" href="noticeWrite">글쓰기</a>
									<!--  관리자인 경우만 글쓰기 버튼 활성화 -->
								</c:when>
							</c:choose>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
	<%@include file="../../bottom.jsp"%>

</body>
</html>