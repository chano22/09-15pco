<!-- 고객센터 : 1:1문의하기 -->


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%@ page import="com.javalec.spring_mybatis.dto.qnaCategoryDto"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="../../top.jsp"%>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>1:1 문의</title>
<link
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css"
	rel="stylesheet">

<style>
@import
	url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css)
	;


.fa-fw {
	width: 2em;
}

a.menu {
	color: black;
}

a.contents {
	color: black;
}

.nav-pills>li.active>a {
	background: #428bca;
	top: 0px;
}

.nav-pills a:hover {
	color : black;
}

li.active>a {
	top: 0px;
}
</style>

</head>
<body>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script
		src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
	<div class="container">
		<div class="row">
			<div class="col-md-3">
					<ul class="nav nav-pills nav-stacked admin-menu">
						<li><a class="menu" href="notice" data-target-id="notice"><i
								class="fa fa-home fa-fw"></i>공지사항</a></li>
						<li><a class="menu" href="question" data-target-id="question"><i
								class="fa fa-list-alt fa-fw"></i>Q&A</a></li>
						<li class="active"><a class="menu" href="mantoman" data-target-id="mantoman"><i
								class="fa fa-file-o fa-fw"></i>1:1 문의</a></li>
					</ul>
				</div>
			<div class="col-md-9 well well-sm">
				<form class="form-horizontal"
					action="${pageContext.request.contextPath}/mantomanWriteOK"
					method="get">
					<input id="id" name="id" type="hidden" value="${dto.id}">
					<fieldset>
						<legend class="text-center">Contact us</legend>

						<!-- category-->
						<div class="form-group">
							<label class="col-md-2 control-label" for="name">카테고리</label>
							<div class="col-md-9">
								<select class="form-control" id="category" name="category">
									<c:forEach var="list" items="${requestScope.qnaCategoryDto}"
										varStatus="loop">
										<option value="${list.name}">${list.name}</option>
									</c:forEach>
								</select>
							</div>
						</div>

						<!-- Subject input-->
						<div class="form-group">
							<label class="col-md-2 control-label" for="email">제목</label>
							<div class="col-md-9">
								<input id="subject" name="subject" type="text"
									class="form-control">
							</div>
						</div>

						<!-- Message body -->
						<div class="form-group">
							<label class="col-md-2 control-label" for="message">Your
								message</label>
							<div class="col-md-9">
								<textarea class="form-control" id="message" name="message"
									rows="20"></textarea>
							</div>
						</div>

						<!-- File body -->
						<div class="form-group">
							<label class="col-md-2 control-label" for="exampleInputFile">파일 업로드</label> 
							<input type="file" id="File">
						</div>

						<!-- Form actions -->
						<div class="form-group">
							<div class="col-md-3 pull-right">
								<button type="submit" class="btn btn-primary btn-lg">글쓰기</button>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
	<%@include file="../../bottom.jsp"%>
</body>
</html>