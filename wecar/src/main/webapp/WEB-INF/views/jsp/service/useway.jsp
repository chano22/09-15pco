<!-- 서비스안내  : 이용방법 -->

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="../../top.jsp"%>

<style>
@import
	url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css)
	;

body {
	margin-top: 20px;
}

.fa-fw {
	width: 2em;
}
</style>

<!-- <script type="text/javascript">
	$(document).ready(function() {
		var navItems = $('.admin-menu li > a');
		var navListItems = $('.admin-menu li');
		var allWells = $('.admin-content');
		var allWellsExceptFirst = $('.admin-content:not(:first)');

		allWellsExceptFirst.hide();
		navItems.click(function(e) {
			e.preventDefault();
			navListItems.removeClass('active');
			$(this).closest('li').addClass('active');

			allWells.hide();
			var target = $(this).attr('data-target-id');
			$('#' + target).show();
		});
	});
</script> -->

<div class="container">
	<div class="row">
		<div class="col-md-3">
			<ul class="nav nav-pills nav-stacked admin-menu">
				<li class="active"><a href="#" data-target-id="way"><i
						class="fa fa-home fa-fw"></i>이용방법</a></li>
				<li><a href="useway"
					data-target-id="rule"><i class="fa fa-list-alt fa-fw"></i>이용규칙</a></li>
				<li><a href="http://www.jquery2dotnet.com"
					data-target-id="guide"><i class="fa fa-file-o fa-fw"></i>쏘카 가이드</a></li>
				<li><a href="http://www.jquery2dotnet.com"
					data-target-id="impact"><i class="fa fa-bar-chart-o fa-fw"></i>쏘카
						임팩트</a></li>

			</ul>
		</div>
		<div class="col-md-9 well admin-content" id="way">
			<h2>쏘카 이용방법</h2>
			<p>
				<font size="3"> 가입에서 예약 사용까지 간편하게 이용할 수 있는 편리하고 경제적인 쏘카의
					이용방법을 알려드립니다.<br> 쏘카와 함께 스마트 카셰어링을 경험해보세요!
				</font>
			<p>
				Here's the original one from BhaumikPatel: <a
					href="http://bootsnipp.com/snippets/featured/vertical-admin-menu"
					target="_BLANK">Vertical Admin Menu</a> <br> Thank you Baumik!
			</p>
		</div>
<!-- 		<div class="col-md-9 well admin-content" id="rule">
			<h3>쏘카 이용규칙</h3>
			<p></p>
		</div>
		<div class="col-md-9 well admin-content" id="guide">
			<h3>쏘카 가이드</h3>
			<p></p>
		</div>
		<div class="col-md-9 well admin-content" id="impact">
			<h3>쏘카 임팩트</h3>
			<p></p>
		</div> -->

		<div align="right">
			<input type="button" value="수정" onclick="window.location='use_update'">
		</div>
	</div>

</div>

<%@include file="../../bottom.jsp"%>

