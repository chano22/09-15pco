<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>	
 
<%@include file="../../top.jsp"%>

 <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet">
    
    	<link rel="stylesheet" href="resources/time_slider/css/jslider.css" type="text/css">
	<link rel="stylesheet" href="resources/time_slider/css/jslider.blue.css" type="text/css">
	<link rel="stylesheet" href="resources/time_slider/css/jslider.plastic.css" type="text/css">
	<link rel="stylesheet" href="resources/time_slider/css/jslider.round.css" type="text/css">
	<link rel="stylesheet" href="resources/time_slider/css/jslider.round.plastic.css" type="text/css">
  <style type="text/css">
    @import 'http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin';
                @import 'http://fonts.googleapis.com/css?family=Anton';

                body {
                    min-height: 500px;
                    background-color: #f3f3f3;
                    color: #333;
                    font: 13px/18px 'Open Sans', arial, sans-serif;
                    -webkit-font-smoothing: antialiased;
                }

                .navbar-nav > li > a {
                    padding-top: 15px;
                    padding-bottom: 15px;
                    line-height: 20px;
                }

                .top-bar {
                    background-color: #fff;
                    margin-bottom: 0px;
                    width: 100%;
                    height: 50px;
                    webkit-box-shadow: rgba(0, 0, 0, 0.14902) 0px 2px 4px 0px;
                    -moz-box-shadow: rgba(0, 0, 0, 0.14902) 0px 2px 4px 0px;
                    box-shadow: rgba(0, 0, 0, 0.14902) 0px 2px 4px 0px;
                }

                .menu-item {
                    line-height: 20px;
                    color: #3f729b;
                    font-weight: bold;
                    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
                    font-size: 13px;
                }

                .menu-item-flexible {
                    max-width: 230px;
                    text-overflow: ellipsis;
                    overflow: hidden;
                    white-space: nowrap;
                }

                .web-logo {
                    color: #3f729b;
                    font-size: 18px;
                    font-weight: 200;
                    line-height: 1;
                    padding-left: 0px;
                }

                @media (max-width: 768px) {
                    .web-logo {
                        padding-left: 15px;
                    }
                }

                .header-profile {
                    color: #3f729b;
                    font-weight: bold;
                    line-height: 1;
                    max-width: 100px;
                    text-overflow: ellipsis;
                    white-space: nowrap;
                    overflow: hidden;
                }

                .navbar-nav > li > a.header-profile {
                    padding-top: 10px;
                    padding-bottom: 10px;
                    line-height: 30px;
                }

                .user-name-box {
                    margin-left: 5px;
                    display: none;
                }

                @media (min-width:768px) {
                    .navbar-nav > li > a.header-profile {
                        padding-right: 5px;
                    }

                    .web-logo {
                        padding-left: 15px;
                    }
                }

                #bs-navbar {
                    background-color: #fff;
                }

                @media (max-width:767px) {
                    .navbar-nav > li > a.header-profile {
                        max-width: none;
                        text-overflow: inherit;
                        white-space: inherit;
                        overflow: inherit;
                        padding-top: 5px;
                        padding-bottom: 5px;
                        line-height: 25px;
                    }

                    .menu-item {
                        font-size: 14px;
                    }

                    .menu-item-flexible {
                        max-width: none;
                        text-overflow: inherit;
                        overflow: inherit;
                        white-space: inherit;
                    }

                    .user-name-box {
                        font-size: 14px;
                        display: inline-block;
                    }

                    .navbar-nav > li {
                        background-color: #eee;
                    }

                    #bs-navbar {
                        background-color: #eee;
                        font-size: 15px;
                        webkit-box-shadow: rgba(0, 0, 0, 0.14902) 0px 2px 4px 0px;
                        -moz-box-shadow: rgba(0, 0, 0, 0.14902) 0px 2px 4px 0px;
                        box-shadow: rgba(0, 0, 0, 0.14902) 0px 2px 4px 0px;
                    }
                }

                .navbar-toggle .icon-bar {
                    background-color: #3f729b;
                }

                .navbar-toggle {
                    padding-right: 0px;
                }

                @media (max-width: 768px) {
                    .top-bar {
                        padding-left: 0px;
                    }

                    .navbar-brand {
                    }
                }

                .container > .navbar-header,
                .container-fluid > .navbar-header,
                .container > .navbar-collapse,
                .container-fluid > .navbar-collapse {
                    margin-left: 0px;
                    margin-right: 0px;
                }

                /*
                    slide 스타일
                */


                .carousel .item img {
                    width: 100%;
                    height: 420px;
                }

                .carousel .nav {
                    text-align: center;
                    margin: 0 auto;
                    max-width: 1140px;
                }


                    .carousel .nav li img {
                        text-align: center;
                        margin: 0 auto;
                        padding: 15px 0 0 0;
                    }


                .carousel h2 {
                    text-transform: uppercase;
                    font-size: 18px;
                    text-align: center;
                }



                .carousel li.active {
                    background: #90d91b;
                }

                .nav-pills > li.active > a:focus, .nav-pills > li.active > a:hover {
                    color: #fff;
                    background: #90d91b;
                }


                .nav-pills a:hover {
                    z-index: 99;
                    background: #90d91b;
                    color: #fff;
                    position: relative;
                    cursor: pointer;
                }


                li:hover {
                    z-index: 99;
                    background: #90d91b;
                    color: #fff;
                    position: relative;
                    cursor: pointer;
                }


                li.active > a {
                    z-index: 99;
                    background: #90d91b;
                    color: #fff;
                    position: relative;
                    top: -20px;
                    cursor: pointer;
                }


                .nav-pills > li.active > a {
                    z-index: 99;
                    background: #90d91b;
                    color: #fff;
                    position: relative;
                    top: -20px;
                    cursor: pointer;
                }


                .carousel-control i {
                    position: relative;
                    top: 50%;
                    bottom: 0;
                    left: 0;
                    right: 0;
                    width: 100%;
                    font-size: 90px;
                    color: #90d91b;
                    text-align: center;
                    text-shadow: none;
                    filter: none;
                    opacity: 1;
                }


                .carousel-caption {
                    position: absolute;
                    top: 45%;
                    z-index: 10;
                    padding-top: 0px;
                    padding-bottom: 0px;
                    color: #fff;
                }


                    .carousel-caption h3 {
                        font-size: 88px;
                        right: 0;
                        top: 0;
                        left: 0;
                        z-index: 10;
                        color: #90d91b;
                        text-align: center;
                        text-shadow: 5px 5px 0px rgba(0,0,0,.9);
                    }

                .carousel {
                    height: 420px;
                    border-bottom: 10px solid #00d2ff;
                }

                .slide {
                    height: 420px;
                }

                .bottombody {
                    width: 500px;
                    height: 1140px;
                    border: 10px;
                    margin-left: 20%;
                    margin-right: 20%;
                }

                /*
             body적용


         */
                .hide-bullets {
                    list-style: none;
                    margin-left: -40px;
                    margin-top: 20px;
                }

                /*
                     footer
                 */
                .underfooter {
                    height: 300px;
                    width: 1920px;
                    background-color: dimgrey;
                }

                .footer-menu > li {
                    float: left;
                    list-style: none;
                    font-family: Dotum,tahaoma;
                    font-size: 12px;
                    line-height: 1.3em;
                    color: #666;
                }

                .slide {
                    margin-bottom: 30px;
                }

                .carousel-control {
                    opacity: 0;
                }

                    .carousel-control.left, .carousel-control.right {
                        background-image: none;
                    }

                .top-wrapper {
                    border-top: 4px solid #00d2ff;
                }

                    .top-wrapper > #MeuCarousel > .carousel-inner {
                        border-bottom: 4px solid #00d2ff;
                    }


                .col-sm-3:hover {
                    background: none;
                }

                .first-dark {
                    color: #e6e6e6;
                }

                .hr-dark {
                    border-bottom: 1px solid #e6e6e6;
                }

                .first-bright {
                    color: #00d2ff;
                }

                .hr-bright {
                    border-bottom: 1px solid #00d2ff;
                }
                      .stepwizard-step p {
    margin-top: 10px;
}

.stepwizard-row {
    display: table-row;
}

.stepwizard {
    display: table;
    width: 100%;
    position: relative;
}

.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}

.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;

}

.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}

.btn-circle {
  width: 30px;
  height: 30px;
  text-align: center;
  padding: 6px 0;
  font-size: 12px;
  line-height: 1.428571429;
  border-radius: 15px;
}
.btn-primary{
	margin-top: 5px;
}
.map_wrap {position:relative;width:100%;height:350px;}
    .title {font-weight:bold;display:block;}
    .hAddr {position:absolute;left:10px;top:10px;border-radius: 2px;background:#fff;background:rgba(255,255,255,0.8);z-index:1;padding:5px;}
    #centerAddr {display:block;margin-top:2px;font-weight: normal;}
    .bAddr {padding:5px;text-overflow: ellipsis;overflow: hidden;white-space: nowrap;}

.showcars{
width:100%;
}
.righttime,.lefttime{
	background: blue;
}

    </style>
    
    	<style type="text/css" media="screen">
	 .layout { padding: 50px; font-family: Georgia, serif; }
	 .layout-slider { margin-bottom: 60px; width: 50%; }
	 .layout-slider-settings { font-size: 12px; padding-bottom: 10px; }
	 .layout-slider-settings pre { font-family: Courier; }
	</style>
</style>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
    </script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>


       <div id="MeuCarousel" class="carousel slide" data-ride="carousel">

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <img src="http://fakeimg.pl/1920x200/">
                    <div class="carousel-caption">
                        <h3>SLIDE 1</h3>
                    </div>
                </div>
                <!-- End Item -->
                <div class="item">
                    <img src="http://fakeimg.pl/1920x200/">
                    <div class="carousel-caption">
                        <h3>SLIDE 2</h3>
                    </div>
                </div>
                <!-- End Item -->
                <div class="item">
                    <img src="http://fakeimg.pl/1920x200/">
                    <div class="carousel-caption">
                        <h3>SLIDE 3</h3>
                    </div>
                </div>
                <!-- End Item -->
                <div class="item">
                    <img src="http://fakeimg.pl/1920x200/">
                    <div class="carousel-caption">
                        <h3>SLIDE 4</h3>
                    </div>
                </div>
                <!-- End Item -->

                <div class="item">
                    <img src="http://fakeimg.pl/1920x200/">
                    <div class="carousel-caption">
                        <h3>SLIDE 5</h3>
                    </div>
                </div>
                <!-- End Item -->

                <div class="item">
                    <img src="http://fakeimg.pl/1920x200/">
                    <div class="carousel-caption">
                        <h3>SLIDE 6</h3>
                    </div>
                </div>
                
                
                
                
                
                <!-- End Item -->
            </div>
            <!-- End Carousel Inner -->


            <a href="#MeuCarousel" class="left carousel-control" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
            <a href="#MeuCarousel" class="right carousel-control" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>

        </div>
    </div>

    <!-- body-->
    
    <div class="container">
	  <div class="map_wrap">
	    <div id="map" style="width:100%;height:100%;position:relative;overflow:hidden;"></div>
	    <div class="hAddr">
	        <span class="title">지도중심기준 행정동 주소정보</span>
	        <span id="centerAddr"></span>
	    </div>
	  </div>
	  
   <script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=5a66d317e1afa3122d0330d2cfe755cf&libraries=services"></script>
	<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=5a66d317e1afa3122d0330d2cfe755cf"></script>
	     <!-- bin/jquery.slider.min.js -->
	<script type="text/javascript" src="resources/time_slider/js/jshashtable-2.1_src.js"></script>
	<script type="text/javascript" src="resources/time_slider/js/jquery.numberformatter-1.2.3.js"></script>
	<script type="text/javascript" src="resources/time_slider/js/tmpl.js"></script>
	<script type="text/javascript" src="resources/time_slider/js/jquery.dependClass-0.1.js"></script>
	<script type="text/javascript" src="resources/time_slider/js/draggable-0.1.js"></script>
	<script type="text/javascript" src="resources/time_slider/js/jquery.slider.js"></script>
 
	
	
	
	
	
	
	
	
	<script>
					var position_x,postion_y;
					var jqjibun;
					var MARKER_WIDTH = 33, // 기본, 클릭 마커의 너비 //클릭 마커의 가로
				    MARKER_HEIGHT = 36, // 기본, 클릭 마커의 높이  //클릭마커의 세로
				    OFFSET_X = 12, // 기본, 클릭 마커의 기준 X좌표//클릭마커의 좌표왼쪽
				    OFFSET_Y = MARKER_HEIGHT, // 기본, 클릭 마커의 기준 Y좌표//클릭마커의 아래로 y좌표 
				    OVER_MARKER_WIDTH = 40, // 오버 마커의 너비 //hover시 마크 가로
				    OVER_MARKER_HEIGHT = 42, // 오버 마커의 높이//hover시 마커 세로
				    OVER_OFFSET_X = 13, // 오버 마커의 기준 X좌표//hover시 마커 왼쪽
				    OVER_OFFSET_Y = OVER_MARKER_HEIGHT, // 오버 마커의 기준 Y좌표//hover 마커 아래로 y표
				    SPRITE_MARKER_URL ="resources/Marker_sprite/modifymarker.png", // 스프라이트 마커 이미지 URL
				    SPRITE_WIDTH = 126, // 스프라이트 이미지 너비
				    SPRITE_HEIGHT = 146, // 스프라이트 이미지 높이
				    SPRITE_GAP = 10; // 스프라이트 이미지에서 마커간 간격
		//여기까진 ㄱㅊ
			var markerSize = new daum.maps.Size(MARKER_WIDTH, MARKER_HEIGHT), // 기본, 클릭 마커의 크기
			    markerOffset = new daum.maps.Point(OFFSET_X, OFFSET_Y), // 기본, 클릭 마커의 기준좌표
			    overMarkerSize = new daum.maps.Size(OVER_MARKER_WIDTH, OVER_MARKER_HEIGHT), // 오버 마커의 크기
			    overMarkerOffset = new daum.maps.Point(OVER_OFFSET_X, OVER_OFFSET_Y), // 오버 마커의 기준 좌표
			    spriteImageSize = new daum.maps.Size(SPRITE_WIDTH, SPRITE_HEIGHT); // 스프라이트 이미지의 크기
			
			var positions = [  // 마커의 위치
				    new daum.maps.LatLng(33.44945, 126.56967),
			        new daum.maps.LatLng(33.450579, 126.56956),
			        new daum.maps.LatLng(33.4506468, 126.5707),
			        new daum.maps.LatLng(33.44945, 126.5667),
			        new daum.maps.LatLng(33.4508, 126.5707)
			    
			    ],
			    selectedMarker = null; // 클릭한 마커를 담을 변수
			
			var mapContainer = document.getElementById('map'), // 지도를 표시할 div
			    mapOption = { 
			        center: new daum.maps.LatLng(33.450701, 126.570667), // 지도의 중심좌표
			        level: 3 // 지도의 확대 레벨
			    };
			
			var map = new daum.maps.Map(mapContainer, mapOption); // 지도를 생성합니다
		    var geocoder = new daum.maps.services.Geocoder();
			// 지도 위에 마커를 표시합니다
			var normalcheck,clickcheck;
			for (var i = 0; i < positions.length;  i++) {
			    var gapX = (MARKER_WIDTH + SPRITE_GAP), // 스프라이트 이미지에서 마커로 사용할 이미지 X좌표 간격 값
			        originY = (MARKER_HEIGHT + SPRITE_GAP), // 스프라이트 이미지에서 기본, 클릭 마커로 사용할 Y좌표 값
			        overOriginY = (OVER_MARKER_HEIGHT + SPRITE_GAP), // 스프라이트 이미지에서 오버 마커로 사용할 Y좌표 값
			        normalOrigin = new daum.maps.Point(0, originY), // 스프라이트 이미지에서 기본 마커로 사용할 영역의 좌상단 좌표
			        clickOrigin = new daum.maps.Point(gapX, originY), // 스프라이트 이미지에서 마우스오버 마커로 사용할 영역의 좌상단 좌표
			        overOrigin = new daum.maps.Point(gapX * 2, overOriginY); // 스프라이트 이미지에서 클릭 마커로 사용할 영역의 좌상단 좌표
			        console.log(gapX);
			    // 마커를 생성하고 지도위에 표시합니다
			    addMarker(positions[i], normalOrigin, overOrigin, clickOrigin);
			}
			
			// 마커를 생성하고 지도 위에 표시하고, 마커에 mouseover, mouseout, click 이벤트를 등록하는 함수입니다
			function addMarker(position, normalOrigin, overOrigin, clickOrigin) {
			
			    // 기본 마커이미지, 오버 마커이미지, 클릭 마커이미지를 생성합니다
			    var normalImage = createMarkerImage(markerSize, markerOffset, normalOrigin),
			        overImage = createMarkerImage(overMarkerSize, overMarkerOffset, overOrigin),
			        clickImage = createMarkerImage(markerSize, markerOffset, clickOrigin);
			    
			    // 마커를 생성하고 이미지는 기본 마커 이미지를 사용합니다
			    var marker = new daum.maps.Marker({
			        map: map,
			        position: position,
			        image: normalImage
			    });
				normalcheck=normalImage;
				clickcheck=clickImage;
			    // 마커 객체에 마커아이디와 마커의 기본 이미지를 추가합니다
			    marker.normalImage = normalImage;
			
			    // 마커에 mouseover 이벤트를 등록합니다
			    daum.maps.event.addListener(marker, 'mouseover', function() {
							//console.log(marker.getPosition());
			        // 클릭된 마커가 없고, mouseover된 마커가 클릭된 마커가 아니면
			        // 마커의 이미지를 오버 이미지로 변경합니다
			        if (!selectedMarker || selectedMarker !== marker) {
			            marker.setImage(overImage);
			        }
			    });
			
			    // 마커에 mouseout 이벤트를 등록합니다
			    daum.maps.event.addListener(marker, 'mouseout', function() {
			
			        // 클릭된 마커가 없고, mouseout된 마커가 클릭된 마커가 아니면
			        // 마커의 이미지를 기본 이미지로 변경합니다
			        if (!selectedMarker || selectedMarker !== marker) {
			            marker.setImage(normalImage);
			        }
			    });
				//주소 를 가져오기위해서 geodcoder를 선언함.

				
				
				   var infowindow = new daum.maps.InfoWindow({	   
					   zindex:1,
					   removable : true   
				   }); // 클릭한 위치에 대한 주소를 표시할 인포윈도우입니다
				    searchAddrFromCoords(map.getCenter(), displayCenterInfo);

				
				    daum.maps.event.addListener(map, 'idle', function() {
				        searchAddrFromCoords(map.getCenter(), displayCenterInfo);
				    });

			        
				
				// 마커에 click 이벤트를 등록합니다
			    daum.maps.event.addListener(marker, 'click', function() {
			
				    // 클릭된 마커가 없고, click 마커가 클릭된 마커가 아니면
			        // 마커의 이미지를 클릭 이미지로 변경합니다
			        if (!selectedMarker || selectedMarker !== marker) {
			//여기서 주소를 추가한다.
			
			
					    searchDetailAddrFromCoords(marker.getPosition(), function(result, status) {
					        if (status === daum.maps.services.Status.OK) {
					        	console.log(marker.getPosition());
					        	var positions=marker.getPosition();
					            position_x=positions.getLat();
								position_y=positions.getLng();
								console.log(position_x);
								console.log(position_y);
							
					        	var detailAddr = !!result[0].road_address ? '<div>도로명주소 : ' + result[0].road_address.address_name + '</div>' : '';
					            detailAddr += '<div>지번 주소 : ' + result[0].address.address_name + '</div><button id="res_btn">클릭미</button>'+'<div>'+'</div>';
					            
					            var content = '<div class="bAddr">' +
					                            '<span class="title">법정동 주소정보</span>' + 
					                            detailAddr + 
					                        '</div>';
								jqjibun=result[0].address.address_name
					            // 마커를 클릭한 위치에 표시합니다 
								
					            // 인포윈도우에 클릭한 위치에 대한 법정동 상세 주소정보를 표시합니다
					            infowindow.setContent(content);
					            infowindow.open(map, marker);

							    
					        }   
					    });
						
			
			
			
			            // 클릭된 마커 객체가 null이 아니면
			            // 클릭된 마커의 이미지를 기본 이미지로 변경하고
			            !!selectedMarker && selectedMarker.setImage(selectedMarker.normalImage);
			            
			            // 현재 클릭된 마커의 이미지는 클릭 이미지로 변경합니다
			            marker.setImage(clickImage);
			        }
			
			        // 클릭된 마커를 현재 클릭된 마커 객체로 설정합니다
			        selectedMarker = marker;

				//    console.log(selectedMarker.getImage());
				
		            
			        //    console.log(clickImage);
	        
			    });		    
			}
	    
			
		    
        //    console.log(normalImage);
    	// (!!marker.getImage() && normalImage) && infowindow.close(); 
	      /*       if((!!selectedMarker.getImage())==normalImage)
	           		{
	           		console.log('같음d');
	           		}
	           	else
	           		{
	           		infowindow.close();
	           		console.log('다름');
	           		}
		 */
			

			function searchAddrFromCoords(coords, callback) {
			    // 좌표로 행정동 주소 정보를 요청합니다
			    geocoder.coord2RegionCode(coords.getLng(), coords.getLat(), callback);         
			}

			function displayCenterInfo(result, status) {
			    if (status === daum.maps.services.Status.OK) {
			        var infoDiv = document.getElementById('centerAddr');
			        infoDiv.innerHTML = result[0].address_name;
			        
			    }    
			}
			
			function searchDetailAddrFromCoords(coords, callback) {
			    // 좌표로 법정동 상세 주소 정보를 요청합니다
			    geocoder.coord2Address(coords.getLng(), coords.getLat(), callback);
			}
			
			// MakrerImage 객체를 생성하여 반환하는 함수입니다
			function createMarkerImage(markerSize, offset, spriteOrigin) {
			    var markerImage = new daum.maps.MarkerImage(
			        SPRITE_MARKER_URL, // 스프라이트 마커 이미지 URL
			        markerSize, // 마커의 크기
			        {
			            offset: offset, // 마커 이미지에서의 기준 좌표
			            spriteOrigin: spriteOrigin, // 스트라이프 이미지 중 사용할 영역의 좌상단 좌표
			            spriteSize: spriteImageSize // 스프라이트 이미지의 크기
			        }
			    );
			    
			    return markerImage;
			}


			$(document).on('click','#res_btn',function(){
			
				$('.table').remove();
				$.ajax({
					type: "POST",
					url: "getcars",
					data:{"position_x":position_x,"position_y":position_y},
					success: function(result){
					
						var output='';
						output+='<table class="table">';
						output+='<thread>';
						output+='<tr>';				
						output+='<th>쏘카존</th>'
						output+='<th>차량</th>'
						output+='<th>주차요금</th>';
						output+='</tr>';	
						output+='</thead>';
						output+='</tbody>';
					    for(var i=0; i<result.list.length ; i++)
							{
					    	
					    	 $('.jslider-value > span').text();
								

					 	    output+='<div class="block">';
							output+='<tr>';
						    //	output+='<th rowspan="2">'
											output+='<td rowspan="2">'+jqjibun +'</td><td><img src="resources/Car_Images/'+result.list[i].car_name+'.png"></td>'
									    	console.log(result.list[0].car_name);
																	
											output+='<td>'+result.list[i].car_name+'</td>'
											output+='<td>SO회원 할인가</td><td>'+result.list[i].car_price+'</td><td><button class="reserve">예약</button></td>'
											if(result.list[i].reser_did==1)
												var checkreser='예약 되어있습니다.';
											else
												var checkreser='예약 가능합니다.'
											output+='<tr><td colspan="3"><div class="inhere"><div class="layout-slider"><input id="Slider5" type="slider" name="area" value="600;720" class="time-slider" /></div><td colspan="2" class="checkreser">'+checkreser+'</td></div>'
											output+='</td></tr>'
						    output+='</tr>';
							output+='</div>';
										    
							
							}
					     console.log()
					     output+='</tbody>';
					     output+='</table>';
						$('#showcars').append(output);
						   $(".time-slider").slider({ from: 480, to: 1020, step: 15, dimension: '', scale: ['8:00', '9:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00'], limits: false, calculate: function( value ){
						        var hours = Math.floor( value / 60 );
						        var mins = ( value - hours*60 );
						      
						        return (hours < 10 ? "0"+hours : hours) + ":" + ( mins == 0 ? "00" : mins );
						      }})	
						      
					     $(".time-slider").slider(600, 10,15);
						   $('.inhere').each(function(index,item)
						   {
							   $(item).attr('id','inhere-'+index);
						   });
						   $('.reserve').each(function(index,item)
						   {
								   console.log('test');
								   $(item).attr('id','reserve-'+index);
					       });
						   $('.checkreser').each(function(index,item)
								   {
										   console.log('test');
										   $(item).attr('id','checkreser-'+index);
							       });	   
							   $(document).on('click','.reserve',function(){
							        var index= $(this).index(".reserve");
							
								        
								        
								 	   var lefttime=$('#inhere-'+index).find('.left-time').text();
									   var righttime=$('#inhere-'+index).find('.right-time').text();
								
										console.log("나오냐??ㅇㄻㄴㅇㅁㄹㅇㄴㅁㄹㄴㅁㅇㄹㄴㅁㅇㄹㄴㅁㅇㄹㄴㅁㅇㄹㅇㄴㄻㅇㄴㄹ");
								        
								/*         if (lefttime < 10)
							            {
								        	lefttime = '0' + lefttime;
							            }
							            if(righttime < 10)
							            {
							            	righttime = '0' + righttime;
							            } */
							            var lefthour=lefttime.substring(0,2);
							            var leftminutes=lefttime.substring(3,5);
							            var righthour=righttime.substring(0,2);
							            var rightminutes=righttime.substring(3,5);
							            var left_time=new Date();
							            left_time.setHours(lefthour);
							            left_time.setMinutes(leftminutes);
							           	var right_time=new Date();
							           	right_time.setHours(righthour);
							           	right_time.setMinutes(rightminutes);
							            var temp=(right_time-left_time)/(60*1000);
							         
							        	
							        	   var beforetime = new Date();
										   beforetime.setHours(lefthour);
										   beforetime.setMinutes(leftminutes);
										   var aftertime=new Date()
										   aftertime.setHours(righthour);
										   aftertime.setMinutes(rightminutes);
										   
										   var timestring1 =
										     leadingZeros(beforetime.getFullYear(), 4) + '/' +
										     leadingZeros(beforetime.getMonth() + 1, 2) + '/' +
										     leadingZeros(beforetime.getDate(), 2) + ' ' +

										     leadingZeros(beforetime.getHours(), 2) + ':' +
										     leadingZeros(beforetime.getMinutes(), 2) + ':' +
										     leadingZeros(beforetime.getSeconds(), 2);
										     var timestring2 =
										     leadingZeros(aftertime.getFullYear(), 4) + '/' +
										     leadingZeros(aftertime.getMonth() + 1, 2) + '/' +
										     leadingZeros(aftertime.getDate(), 2) + ' ' +

										     leadingZeros(aftertime.getHours(), 2) + ':' +
										     leadingZeros(aftertime.getMinutes(), 2) + ':' +
										     leadingZeros(aftertime.getSeconds(), 2);

											
										    
										   
											  
										   
											function leadingZeros(n, digits) {
									    		  var zero = '';
									    		  n = n.toString();

									    		  if (n.length < digits) {
									    		    for (i = 0; i < digits - n.length; i++)
									    		      zero += '0';
									    		  }
									    		  return zero + n;
									    		}
											
							        	
							           var id2="${dto.id}";
								  		//console.log(id2);
								     
								     		console.log(result.list[index].reser_did);
								     		if(result.list[index].reser_did==1)
								     		{
								     	 	  alert("이미 예약되어 있는 상태입니다.");
										      return;
								     		}
								     	else if(result.list[index].reser_did==0)
								     		{
								     		//data:{"position_x":position_x,"position_y":position_y},		
									     	    $.ajax({
										        	
										        	type: "POST",
													url: "getindex",
													data:{"userid":id2,"reservation_index":result.list[index].reservation_final_index,
														"leftttime":timestring1,
														"righttime":timestring2,
														
														//"leftttime":$('#inhere-'+index).find('.left-time').text(),
														//"righttime":$('#inhere-'+index).find('.right-time').text(),
														"price":(temp/30)*result.list[index].car_price},
														
													success: function(result){
														if(result==0)
															{
																alert("예약이 완료되었습니다!");
																location.reload();
																 		
															}
														else
															{
																alert("예약에 싶래했습니다.!");
															}
																			
													}
										        });
									 	  
								     		
								     		
								     		}
											  
								        
								       
								      
								        //console.log($('#inhere-'+index).find('.left-time').text());
								    	// sole.log($('#inhere-'+index).find('.left-time').text());
										 // console.log($('#inhere-'+index).find('.right-time').text());
										 
										// if($('#inhere-'+index).find('.left-time').text()<hour && $('#inhere-'+index).find('.right-time').text() < ) 
										  
								         /*
										  if(result.list[index].reser_did==1)
								        	  alert("이미 예약되어 있는 상태입니다.");
								          else
								        	 {
								        	  alert('예약이 완료되었습니다!');
								        	 }
										  */
			 
							   });
						
						   
						     <!--
						    		  $(document).on('click','#reserve',function(){
								      alert($(this).parent().find("span").text());	
							    	  console.log($(this > '.jslider-value > span').text());
								      console.log()
									  console.log($(this > '.jslider-value jslider-value-to > span').text());
							      });
							      
						      -->
					
								
					
					}
				})
				
		
				console.log(jqjibun);	
			
				
				
				
				
			})
			
			

	</script>
	
	
    
</div>





<div class="container-fluid">
	<div class="showcars" id="showcars">
		
	</div>
</div>



 <script type="text/javascript">



    </script>




<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
 
  
    <script type="text/javascript">
    
  
    $(document).ready(function(){
    	
    

   	$('#MeuCarousel').carousel({
      interval:   4000
   });

   var clickEvent = false;
   $('#MeuCarousel').on('click', '.nav a', function() {
	   console.log('dddd2');
		 clickEvent = true;
         $('.nav li').removeClass('active');
         $(this).parent().addClass('active');
   }).on('slid.bs.carousel', function(e) {
      if(!clickEvent) {
         var count = $('.nav').children().length -1;
         var current = $('.nav li.active');
         current.removeClass('active').next().addClass('active');
         var id = parseInt(current.data('slide-to'));
         if(count == id) {
            $('.nav li').first().addClass('active');
         }
      }
      clickEvent = false;
   });

  
   
});
    
   
    
    
    </script>
    
    
    
  
  
</body>
