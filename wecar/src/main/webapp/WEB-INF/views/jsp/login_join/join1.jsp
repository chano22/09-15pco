<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<!-- This file has been downloaded from Bootsnipp.com. Enjoy! -->
<title>Static Top Navbar - Bootsnipp.com</title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<link
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css"
	rel="stylesheet">
<style type="text/css">
@import
	'http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin'
	;

@import 'http://fonts.googleapis.com/css?family=Anton';

body {
	min-height: 500px;
	background-color: #f3f3f3;
	color: #333;
	font: 13px/18px 'Open Sans', arial, sans-serif;
	-webkit-font-smoothing: antialiased;
}

.navbar-nav>li>a {
	padding-top: 15px;
	padding-bottom: 15px;
	line-height: 20px;
}

.top-bar {
	background-color: #fff;
	margin-bottom: 0px;
	width: 100%;
	height: 50px;
	webkit-box-shadow: rgba(0, 0, 0, 0.14902) 0px 2px 4px 0px;
	-moz-box-shadow: rgba(0, 0, 0, 0.14902) 0px 2px 4px 0px;
	box-shadow: rgba(0, 0, 0, 0.14902) 0px 2px 4px 0px;
}

.menu-item {
	line-height: 20px;
	color: #3f729b;
	font-weight: bold;
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	font-size: 13px;
}

.menu-item-flexible {
	max-width: 230px;
	text-overflow: ellipsis;
	overflow: hidden;
	white-space: nowrap;
}

.web-logo {
	color: #3f729b;
	font-size: 18px;
	font-weight: 200;
	line-height: 1;
	padding-left: 0px;
}

@media ( max-width : 768px) {
	.web-logo {
		padding-left: 15px;
	}
}

.header-profile {
	color: #3f729b;
	font-weight: bold;
	line-height: 1;
	max-width: 100px;
	text-overflow: ellipsis;
	white-space: nowrap;
	overflow: hidden;
}

.navbar-nav>li>a.header-profile {
	padding-top: 10px;
	padding-bottom: 10px;
	line-height: 30px;
}

.user-name-box {
	margin-left: 5px;
	display: none;
}

@media ( min-width :768px) {
	.navbar-nav>li>a.header-profile {
		padding-right: 5px;
	}
	.web-logo {
		padding-left: 15px;
	}
}

#bs-navbar {
	background-color: #fff;
}

@media ( max-width :767px) {
	.navbar-nav>li>a.header-profile {
		max-width: none;
		text-overflow: inherit;
		white-space: inherit;
		overflow: inherit;
		padding-top: 5px;
		padding-bottom: 5px;
		line-height: 25px;
	}
	.menu-item {
		font-size: 14px;
	}
	.menu-item-flexible {
		max-width: none;
		text-overflow: inherit;
		overflow: inherit;
		white-space: inherit;
	}
	.user-name-box {
		font-size: 14px;
		display: inline-block;
	}
	.navbar-nav>li {
		background-color: #eee;
	}
	#bs-navbar {
		background-color: #eee;
		font-size: 15px;
		webkit-box-shadow: rgba(0, 0, 0, 0.14902) 0px 2px 4px 0px;
		-moz-box-shadow: rgba(0, 0, 0, 0.14902) 0px 2px 4px 0px;
		box-shadow: rgba(0, 0, 0, 0.14902) 0px 2px 4px 0px;
	}
}

.navbar-toggle .icon-bar {
	background-color: #3f729b;
}

.navbar-toggle {
	padding-right: 0px;
}

@media ( max-width : 768px) {
	.top-bar {
		padding-left: 0px;
	}
	.navbar-brand {
		
	}
}

.container>.navbar-header, .container-fluid>.navbar-header, .container>.navbar-collapse,
	.container-fluid>.navbar-collapse {
	margin-left: 0px;
	margin-right: 0px;
}

/*
                    slide 스타일
                */
.carousel .item img {
	width: 100%;
	height: 420px;
}

.carousel .nav {
	text-align: center;
	margin: 0 auto;
	max-width: 1140px;
}

.carousel .nav li img {
	text-align: center;
	margin: 0 auto;
	padding: 15px 0 0 0;
}

.carousel h2 {
	text-transform: uppercase;
	font-size: 18px;
	text-align: center;
}

.carousel li.active {
	background: #90d91b;
}

.nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
	color: #fff;
	background: #90d91b;
}

.nav-pills a:hover {
	z-index: 99;
	background: #90d91b;
	color: #fff;
	position: relative;
	cursor: pointer;
}

li:hover {
	z-index: 99;
	background: #90d91b;
	color: #fff;
	position: relative;
	cursor: pointer;
}

li.active>a {
	z-index: 99;
	background: #90d91b;
	color: #fff;
	position: relative;
	top: -20px;
	cursor: pointer;
}

.nav-pills>li.active>a {
	z-index: 99;
	background: #90d91b;
	color: #fff;
	position: relative;
	top: -20px;
	cursor: pointer;
}

.carousel-control i {
	position: relative;
	top: 50%;
	bottom: 0;
	left: 0;
	right: 0;
	width: 100%;
	font-size: 90px;
	color: #90d91b;
	text-align: center;
	text-shadow: none;
	filter: none;
	opacity: 1;
}

.carousel-caption {
	position: absolute;
	top: 45%;
	z-index: 10;
	padding-top: 0px;
	padding-bottom: 0px;
	color: #fff;
}

.carousel-caption h3 {
	font-size: 88px;
	right: 0;
	top: 0;
	left: 0;
	z-index: 10;
	color: #90d91b;
	text-align: center;
	text-shadow: 5px 5px 0px rgba(0, 0, 0, .9);
}

.carousel {
	height: 420px;
	border-bottom: 10px solid #00d2ff;
}

.slide {
	height: 420px;
}

.bottombody {
	width: 500px;
	height: 1140px;
	border: 10px;
	margin-left: 20%;
	margin-right: 20%;
}

/*
             body적용


         */
.hide-bullets {
	list-style: none;
	margin-left: -40px;
	margin-top: 20px;
}

/*
                     footer
                 */
.underfooter {
	height: 300px;
	width: 1920px;
	background-color: dimgrey;
}

.footer-menu>li {
	float: left;
	list-style: none;
	font-family: Dotum, tahaoma;
	font-size: 12px;
	line-height: 1.3em;
	color: #666;
}

.slide {
	margin-bottom: 30px;
}

.carousel-control {
	opacity: 0;
}

.carousel-control.left, .carousel-control.right {
	background-image: none;
}

.top-wrapper {
	border-top: 4px solid #00d2ff;
}

.top-wrapper>#MeuCarousel>.carousel-inner {
	border-bottom: 4px solid #00d2ff;
}

.col-sm-3:hover {
	background: none;
}

.first-dark {
	color: #e6e6e6;
}

.hr-dark {
	border-bottom: 1px solid #e6e6e6;
}

.first-bright {
	color: #00d2ff;
}

.hr-bright {
	border-bottom: 1px solid #00d2ff;
}

.stepwizard-step p {
	margin-top: 10px;
}

.stepwizard-row {
	display: table-row;
}

.stepwizard {
	display: table;
	width: 100%;
	position: relative;
}

.stepwizard-step button[disabled] {
	opacity: 1 !important;
	filter: alpha(opacity = 100) !important;
}

.stepwizard-row:before {
	top: 14px;
	bottom: 0;
	position: absolute;
	content: " ";
	width: 100%;
	height: 1px;
	background-color: #ccc;
	z-order: 0;
}

input[type=radio] {
	margin-left: 10px;
}

.stepwizard-step {
	display: table-cell;
	text-align: center;
	position: relative;
}

.btn-circle {
	width: 30px;
	height: 30px;
	text-align: center;
	padding: 6px 0;
	font-size: 12px;
	line-height: 1.428571429;
	border-radius: 15px;
}

#checklogintext {
	display: inline;
	width: 40%;
}

#checklogin {
	width: 20%;
	vertical-align: unset;
}

.control-label {
	display: block;
}


#birthday {
	width: 25%;
	display: inline;
}

#password, #check_password {
	width: 40%;
}

#phone1, #phone2, #phone3 {
	width: 20%;
	display: inline;
}

.hr-inline {
	display: inline;
	vertical-align: bottom;
}

#phone1, #pone2, #phone3 {
	width: 20%;
	display: inline;
}

#adress_button {
	width: 30%;
	display: inline;
}

#sample4_postcode {
	width: 30%;
	display: inline;
}
#datepicker1,#datepicker2,#datepicker3{
	width:30%
}
#sample4_roadAddress, #sample4_jibunAddress {
	width: 50%;
}

#password_comment, .form-password, #check_password {
	display: inline;
}
#sel1{
	display: inline;
	width:20%;
}

#li_second,#li_third,#li_first{
	display: inline;
	width:20%;
}
.form-password {
	margin-left: 5px;
}

</style>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
	
</script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="top-wrapper">
		<header class="navbar navbar-static-top top-bar" id="top"
			role="banner">
		<div class="container">
			<div class="navbar-header">
				<button class="navbar-toggle collapsed" type="button"
					data-toggle="collapse" data-target="#bs-navbar"
					aria-controls="bs-navbar" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand web-logo" href="http://meaww.com"
					style="padding: 10px">Google</a>
			</div>
			<nav id="bs-navbar"
				class="collapse navbar-collapse bs-navbar-collapse"
				role="navigation">
			<ul class="nav navbar-nav">
				<li><a href="/" class="menu-item">서비스안내</a></li>
				<li><a href="/logout" class="menu-item">요금안내</a></li>
				<li><a href="/logout" class="menu-item">고객센터</a></li>
				<li><a href="/logout" class="menu-item">마이페이지</a></li>
				<li><a href="/logout" class="menu-item">쏘카 찾기·예약</a></li>
			</ul>

			<ul class="nav navbar-nav navbar-right">
				<li><a href="#"><span class="glyphicon glyphicon-user"></span>
						Sign Up</a></li>
				<li><a href="#"><span class="glyphicon glyphicon-log-in"></span>
						Login</a></li>
			</ul>
			</nav>

		</div>
		</header>




		<!--
            slide복사

            -->
		<div id="MeuCarousel" class="carousel slide" data-ride="carousel">

			<!-- Wrapper for slides -->
			<div class="carousel-inner">
				<div class="item active">
					<img src="http://fakeimg.pl/1920x200/">
					<div class="carousel-caption">
						<h3>SLIDE 1</h3>
					</div>
				</div>
				<!-- End Item -->
				<div class="item">
					<img src="http://fakeimg.pl/1920x200/">
					<div class="carousel-caption">
						<h3>SLIDE 2</h3>
					</div>
				</div>
				<!-- End Item -->
				<div class="item">
					<img src="http://fakeimg.pl/1920x200/">
					<div class="carousel-caption">
						<h3>SLIDE 3</h3>
					</div>
				</div>
				<!-- End Item -->
				<div class="item">
					<img src="http://fakeimg.pl/1920x200/">
					<div class="carousel-caption">
						<h3>SLIDE 4</h3>
					</div>
				</div>
				<!-- End Item -->

				<div class="item">
					<img src="http://fakeimg.pl/1920x200/">
					<div class="carousel-caption">
						<h3>SLIDE 5</h3>
					</div>
				</div>
				<!-- End Item -->

				<div class="item">
					<img src="http://fakeimg.pl/1920x200/">
					<div class="carousel-caption">
						<h3>SLIDE 6</h3>
					</div>
				</div>
				<!-- End Item -->
			</div>
			<!-- End Carousel Inner -->


			<a href="#MeuCarousel" class="left carousel-control"
				data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
			<a href="#MeuCarousel" class="right carousel-control"
				data-slide="next"><span
				class="glyphicon glyphicon-chevron-right"></span></a>

		</div>
	</div>

	<!-- body-->
	<div class="container">
		<div class="stepwizard">
			<div class="stepwizard-row setup-panel">
				<div class="stepwizard-step">
					<a href="#step-1" type="button"
						class="glyphicon glyphicon-user btn btn-primary">1</a>

					<!--     <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a> -->
					<p>Step 1</p>
				</div>
				<div class="stepwizard-step">
					<a href="#step-2" type="button"
						class="glyphicon glyphicon-user btn btn-default"
						disabled="disabled">2</a>
					<p>Step 2</p>
				</div>
			
			</div>
		</div>
		<form name="join12" action="join12" onSubmit="return check();">
			<div class="row setup-content" id="step-1">
				<input type="hidden" name="mypage" value="join">
				<div class="col-xs-12">
					<div class="col-md-12">
						<h3>회원정보</h3>
						<div class="form-group">
							<label class="control-label">이름</label> <input name="name"
								id="name" maxlength="100" type="text" required="required"
								class="form-control" placeholder="이름을 입력하세요." />
						</div>
						<div class="form-group">
							<label class="control-label" id="phone">휴대폰 번호</label> <input
								name="phone1" id="phone1" maxlength="3" type="text"
								required="required" class="form-control"
								placeholder="번호를 입력하세요." />
							<h1 class="hr-inline">-</h1>
							<input name="phone2" id="phone2" maxlength="4" type="text"
								required="required" class="form-control"
								placeholder="번호를 입력하세요." />
							<h1 class="hr-inline">-</h1>
							<input name="phone3" id="phone3" maxlength="4" type="text"
								required="required" class="form-control"
								placeholder="번호를 입력하세요." />
						</div>
						<div class="form-group">
							<label class="control-label">이메일</label> <input name="email"
								id="emailid" maxlength="100" type="text" required="required"
								class="form-control" placeholder="이메일을 입력하세요" />
						</div>
						<div class="form-group">
							<label class="control-label">아이디</label> <input name="id"
								maxlength="100" type="text" id="checklogintext"
								required="required" class="form-control"
								placeholder="아이디를 입력하세요." /> <input type="button" name="yser"
								tabindex="4" id="checklogin" onclick="registerCheckFunction();"
								class="form-control btn" value="중복확인" />
						</div>
						<div class="form-group">
							<label class="control-label">패스워드</label> <input name="pw"
								type="password" id="password" maxlength="200"
								required="required" class="form-control" placeholder="password" />
						</div>
						<div class="form-group">
							<span id="password_comment" style="color: #FF0004;">비밀번호는
								영문,숫자,특수문자(!@$%^&* 만 허용)를 사용하여 6~16자까지, 영문은 대소문자를 구분합니다.</span>
						</div>


						<div class="form-group">
							<label class="control-label">패스워드 재확인</label> <input
								name="checkpasword" type="password" id="check_password"
								maxlength="200" required="required" class="form-control"
								placeholder="username" />
							<div class="form-password">
								<span id="pwmatch" class="glyphicon glyphicon-remove"
									style="color: #FF0004;"></span> Passwords Match
							</div>

						</div>


						<div class="form-group">
							<label class="control-label">주소</label>
							<!--   <input name="lname" maxlength="100" type="text" required="required" class="form-control" placeholder="Enter Last Name" />
                             -->
							<input type="text" name="post" id="sample4_postcode" class="form-control" placeholder="우편번호" required="required" readonly > 
								<input type="button" id="adress_button" onclick="sample4_execDaumPostcode()" class="form-control" value="우편번호 찾기"><br>
								 <input type="text" name="addr1" id="sample4_roadAddress" class="form-control" placeholder="도로명주소" required="required" readonly >
								  <input type="text" name="addr2" id="sample4_jibunAddress" class="form-control" placeholder="지번주소" required="required" readonly >
							<span id="guide" style="color: #999"></span>



						</div>
						<div class="form-group">

							<input type="radio" name="area" value="1">서울 <input
								type="radio" name="area" value="2">경기 <input
								type="radio" name="area" value="5">인천 <input
								type="radio" name="area" value="6">대구 <input
								type="radio" name="area" value="7">광주 <input
								type="radio" name="area" value="8">부산 <input
								type="radio" name="area" value="9">울산 <input
								type="radio" name="area" value="10">강원 <input
								type="radio" name="area" value="11">충북 <input
								type="radio" name="area" value="12">충남 <input
								type="radio" name="area" value="13">전북 <input
								type="radio" name="area" value="14">전남 <input
								type="radio" name="area" value="15">경북 <input
								type="radio" name="area" value="16">경남 <input
								type="radio" name="area" value="17">제주
						</div>
						<button class="btn btn-primary nextBtn btn-lg pull-right"
							type="button">Next</button>
					</div>
				</div>
			</div>
			<div class="row setup-content" id="step-2">
				<div class="col-xs-12">
					<div class="col-md-12">
						<h3>운전면허</h3>
						<div class="form-group">
							<label class="control-label">면허종류</label> <label
								class="radio-inline"> <input type="radio" name="carle"
								value="1">1종 보통
							</label> <label class="radio-inline"> <input type="radio"
								name="carle" value="2">2종 보통
							</label> <label class="radio-inline"> <input type="radio"
								name="carle" value="3">1종 대형
							</label>
						</div>
						<label for="sel1">면허번호</label>
						<div class="form-group" id="li_num">
							<select class="form-control" name="area2" id="sel1">
								<option value="1">서울</option>
								<option value="2">경기</option>
								<option value="3">경기북부</option>
								<option value="4">경기남부</option>
								<option value="5">인천</option>
								<option value="6">대구</option>
								<option value="7">광주</option>
								<option value="8">부산</option>
								<option value="9">울산</option>
								<option value="10">강원</option>
								<option value="11">충북</option>
								<option value="12">충남</option>
								<option value="13">전북</option>
								<option value="14">전남</option>
								<option value="15">경북</option>
								<option value="16">경남</option>
								<option value="17">제주</option>
							</select> <input name="num1" id="li_first" maxlength="2" type="text" required="required" class="form-control" placeholder="첫번째" /> 
								<input name="num2" id="li_second" maxlength="6" type="text"	required="required" class="form-control" placeholder="두번째" />
								 <input name="num3" id="li_third" maxlength="2" type="text" required="required" class="form-control" placeholder="세번째" />
						</div>
						<div class="form-group">
							<label class="control-label">면허 발급일</label> <input type="text"
								name="issue_date" id="datepicker1" maxlength="100"
								required="required" class="form-control"
								placeholder="이름을 입력하세요." />
						</div>
						<div class="form-group">
							<label class="control-label">갱신기간 만료일</label> <input
								name="exp_date" id="datepicker2" maxlength="100" type="text"
								required="required" class="form-control"
								placeholder="이름을 입력하세요." />
						</div>
						<div class="form-group">
							<label class="control-label">생년월일/성별</label> <input
								name="birthday" id="datepicker3" maxlength="100" id="birthday"
								type="text" required="required" class="form-control"
								placeholder="6자리" /> <label class="radio-inline"> <input
								type="radio" name="gender" value="0">남자
							</label> <label class="radio-inline"> <input type="radio"
								name="gender" value="1">여자
							</label>
						</div>
				    	<div class="form-group">
							<h6 style="color: red">· 주민등록 상에 기재된 생년월일을 입력해주세요.</h6>
							<h6 style="color: red">· 만 21세 미만이거나 운전면허 취득 7년 미만일 경우 SO회원
								승인이 불가합니다.</h6>
							<h6 style="color: none">· 타인의 정보를 도용해 가입할 경우 법적 조치가 취해질 수
								있으며, 유효한 정보가 아닐 경우 회원 승인이 거절될 수 있습니다.</h6>

						</div>
						<button class="btn btn-success btn-lg pull-right" type="submit">Register
							Me!</button>
					</div>
				</div>
			</div>
		</form>
	</div>

	<div class="container"></div>

	<div class="underfooter">
		<div class="container">
			<div class="gr">
				<ul class="footer-menu">
					<li><a href=https://www.socar.kr/about class="menu1"
						title="회사소개">회사소개</a></li>
					<li><a href="https://goo.gl/Aqsjr5" title="채용안내"
						target="_blank"
						style="padding: 3px 0 2px; background: none; text-indent: 0;"><img
							src='//web-assets.socar.kr/template/asset/images/common/footer_menu_recruit_new.png'
							alt="채용안내"></a></li>
					<li><a
						href="https://www.dropbox.com/sh/qiypdh3xl3mmktf/AAAlXsdhOZUY3HTPcvcwPXfCa?dl=0"
						target="_blank" class="menu2" title="보도자료">보도자료</a></li>
					<!-- 2015.07.29 -->
					<li><a href=https://www.socar.kr/terms title="회원이용약관"
						style="padding: 3px 0 2px; background: none; text-indent: 0;"><img
							src='//web-assets.socar.kr/template/asset/images/common/footer_menu_member_n.png'
							alt="회원이용약관" /></a></li>
					<li><a href=https://www.socar.kr/privacy class="menu5"
						title="개인정보처리방침">개인정보처리방침</a></li>
					<li><a href=https://www.socar.kr/rent_terms title="자동차대여약관"
						style="padding: 3px 0 2px; background: none; text-indent: 0;"><img
							src='//web-assets.socar.kr/template/asset/images/common/footer_menu_rental_n2.png'
							alt="자동차대여약관" /></a></li>
					<li><a href=https://www.socar.kr/gis_terms
						title="위치기반서비스 이용약관"
						style="padding: 3px 0 2px; background: none; text-indent: 0;"><img
							src='//web-assets.socar.kr/template/asset/images/common/footer_menu_location_n.png'
							alt="위치기반서비스 이용약관" /></a></li>

					<!--// 2015.07.29 -->
				</ul>
				<address>
					<img
						src='//web-assets.socar.kr/template/asset/images/common/footer_address.png?v=3'
						alt="(주) 쏘카 통신판매업 신고 : 제 2011-제주조천-0021호, 정보보호 담당자 : 김명훈, 사업자등록번호 : 616-81-90529  대표자 : 이재용
                Tel : 1661-3315, Fax : 02-6969-9333, 주소 : 제주특별자치도 제주시 도령로 129, 5층 (연동, 드림플라자) 63126" />
				</address>
				<p class="copyright">
					<img style="margin-top: 15px;"
						src='//web-assets.socar.kr/template/asset/images/common/footer_copyright_n2.png'
						alt="Copyright © 2013 SOCAR  All rights reserved." />
				</p>
				<div class="ccm">
					<a href="/impact"><img style="margin-top: 15px;"
						src='//web-assets.socar.kr/template/asset/images/common/ccm.png' /></a>
				</div>
				<div class="eco">
					<a href="/impact"><img style="margin-top: 15px;"
						src='//web-assets.socar.kr/template/asset/images/common/eco.png?v=4' /></a>
				</div>
				<div class="bcorp">
					<a href="/impact"><img style="margin-top: 15px;"
						src='//web-assets.socar.kr/template/asset/images/common/bcorp.jpg' /></a>
				</div>
				<div class="csa">
					<a href="http://carsharing.org/" target="_blank"><img
						style="margin-top: 15px;"
						src='//web-assets.socar.kr/template/asset/images/common/csa.png' /></a>
				</div>
			</div>

			<ul class="sns">
				<li><a href="https://www.facebook.com/socarsharing"
					target="_blank" class="sns1" title="SOCAR Facebook">SOCAR
						Facebook</a></li>
				<li><a href="http://talk.socar.kr" target="_blank" class="sns3"
					title="SOCAR Blog">SOCAR Blog</a></li>
				<li><a href="mailto:info@socar.kr" class="sns4"
					title="info@socar.kr">E-mail</a></li>
			</ul>


		</div>


	</div>

	<!-- 
logincheck modal
 -->

	<div class="modal fade" id="checkModal" role="dialog">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Modal Header</h4>
				</div>
				<div class="modal-body" id="checkMessage"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>



	<link rel="stylesheet"
		href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
	<script type="text/javascript">
	
		var checkid=null;
	
		$(function() {
			$("#datepicker1").datepicker({
				dateFormat : 'yy-mm-dd',
				changeMonth : true,
				changeYear : true,
				yearRange : '1900:2100',
			});
			$("#datepicker2").datepicker({
				dateFormat : 'yy-mm-dd',
				changeMonth : true,
				changeYear : true,
				yearRange : '1900:2100',
			});
			$("#datepicker3").datepicker({
				dateFormat : 'yy-mm-dd',
				changeMonth : true,
				changeYear : true,
				yearRange : '1900:2100',
			});
		});
		// /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/
		function check() {
			
			checkid=true;
			
			console.log($("#sample4_roadAddress").val());
			console.log('나오냐??????????????????????????');
				// 비밀번호(패스워드) 유효성 체크 (문자, 숫자, 특수문자의 조합으로 6~16자리)
			var objUserPasswordRe = document.join12.pw;
			var ObjUserPassword = document.join12.checkpasword;
			var Phone1 = document.join12.phone1;
			var Phone2 = document.join12.phone2;
			var Phone3 = document.join12.phone3;
			var email = document.join12.email;
			var area = document.join12.area2.value;
			var carle = document.join12.carle.value;
			
			

			
			if(!area)
				{
					alert("지역을 선택하세요.")
					return false;
				}
			if(!carle)
			{
				alert("면허를 선택하세요.")
				return false;
				
			}
			if(!checkid)
		    {
				alert("아이디가 이미 있습니다.");
				return false;
			}
			if (!email.value
					.match(/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/)) {
				alert("이메일형식이 잘못됬습니다.");
				return false;
			}
			if (Phone1.value.length < 3 || Phone2.value.length < 4
					|| Phone3.value.length < 4) {
				alert("전화번호를  000-0000-0000 형식으로 입력하세요.");
				return false;
			}
			if (!Phone1.value.match(/[0-9]/) || !Phone2.value.match((/[0-9]/))
					|| !Phone3.value.match(/[0-9]/)) {
				alert("전화번호에 숫자만 입력하세요.");
				return false;
			}
			if (ObjUserPassword.value != objUserPasswordRe.value) {
				alert("입력하신 비밀번호와 비밀번호확인이 일치하지 않습니다");
				return false;
			}
			if (ObjUserPassword.value.length < 6) {
				alert("비밀번호는 영문,숫자,특수문자(!@$%^&* 만 허용)를 사용하여 6~16자까지, 영문은 대소문자를 구분합니다.");
				return false;
			}
			if (!ObjUserPassword.value
					.match(/([a-zA-Z0-9].*[!,@,#,$,%,^,&,*,?,_,~])|([!,@,#,$,%,^,&,*,?,_,~].*[a-zA-Z0-9])/)) {
				alert("비밀번호는 영문,숫자,특수문자(!@$%^&* 만 허용)를 사용하여 6~16자까지, 영문은 대소문자를 구분합니다.");
				return false;
			}
			if (!ObjUserPassword.value
					.match(/([a-zA-Z0-9].*[!,@,#,$,%,^,&,*,?,_,~])|([!,@,#,$,%,^,&,*,?,_,~].*[a-zA-Z0-9])/)) {
				alert("비밀번호는 영문,숫자,특수문자(!@$%^&* 만 허용)를 사용하여 6~16자까지, 영문은 대소문자를 구분합니다.");
				return false;
			}
			//if(ObjUserID.value.indexOf(ObjUserPassword) > -1) {
			// alert("비밀번호에 아이디를 사용할 수 없습니다.");
			// return false;
			//}
			var SamePass_0 = 0; //동일문자 카운트
			var SamePass_1 = 0; //연속성(+) 카운드
			var SamePass_2 = 0; //연속성(-) 카운드
			for (var i = 0; i < ObjUserPassword.value.length; i++) {
				var chr_pass_0 = ObjUserPassword.value.charAt(i);
				var chr_pass_1 = ObjUserPassword.value.charAt(i + 1);
				//동일문자 카운트
				if (chr_pass_0 == chr_pass_1) {
					SamePass_0 = SamePass_0 + 1
				}
				var chr_pass_2 = ObjUserPassword.value.charAt(i + 2);
				//연속성(+) 카운드
				if (chr_pass_0.charCodeAt(0) - chr_pass_1.charCodeAt(0) == 1
						&& chr_pass_1.charCodeAt(0) - chr_pass_2.charCodeAt(0) == 1) {
					SamePass_1 = SamePass_1 + 1
				}
				//연속성(-) 카운드
				if (chr_pass_0.charCodeAt(0) - chr_pass_1.charCodeAt(0) == -1
						&& chr_pass_1.charCodeAt(0) - chr_pass_2.charCodeAt(0) == -1) {
					SamePass_2 = SamePass_2 + 1
				}
			}
			if (SamePass_0 > 1) {
				alert("동일문자를 3번 이상 사용할 수 없습니다.");
				return false;
			}
			if (SamePass_1 > 1 || SamePass_2 > 1) {
				alert("연속된 문자열(123 또는 321, abc, cba 등)을\n 3자 이상 사용 할 수 없습니다.");
				return false;
			}
			return true;
		}

		$("input[type=password]").keyup(function() {
			if ($("#password").val() == $("#check_password").val()) {
				$("#pwmatch").removeClass("glyphicon-remove");
				$("#pwmatch").addClass("glyphicon-ok");
				$("#pwmatch").css("color", "#00A41E");
			} else {
				$("#pwmatch").removeClass("glyphicon-ok");
				$("#pwmatch").addClass("glyphicon-remove");
				$("#pwmatch").css("color", "#FF0004");
			}
		});

		function sample4_execDaumPostcode() {
			new daum.Postcode(
					{
						oncomplete : function(data) {
							// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

							// 도로명 주소의 노출 규칙에 따라 주소를 조합한다.
							// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
							var fullRoadAddr = data.roadAddress; // 도로명 주소 변수
							var extraRoadAddr = ''; // 도로명 조합형 주소 변수

							// 법정동명이 있을 경우 추가한다. (법정리는 제외)
							// 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
							if (data.bname !== ''
									&& /[동|로|가]$/g.test(data.bname)) {
								extraRoadAddr += data.bname;
							}
							// 건물명이 있고, 공동주택일 경우 추가한다.
							if (data.buildingName !== ''
									&& data.apartment === 'Y') {
								extraRoadAddr += (extraRoadAddr !== '' ? ', '
										+ data.buildingName : data.buildingName);
							}
							// 도로명, 지번 조합형 주소가 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
							if (extraRoadAddr !== '') {
								extraRoadAddr = ' (' + extraRoadAddr + ')';
							}
							// 도로명, 지번 주소의 유무에 따라 해당 조합형 주소를 추가한다.
							if (fullRoadAddr !== '') {
								fullRoadAddr += extraRoadAddr;
							}

							// 우편번호와 주소 정보를 해당 필드에 넣는다.
							document.getElementById('sample4_postcode').value = data.zonecode; //5자리 새우편번호 사용
							document.getElementById('sample4_roadAddress').value = fullRoadAddr;
							document.getElementById('sample4_jibunAddress').value = data.jibunAddress;

							// 사용자가 '선택 안함'을 클릭한 경우, 예상 주소라는 표시를 해준다.
							if (data.autoRoadAddress) {
								//예상되는 도로명 주소에 조합형 주소를 추가한다.
								var expRoadAddr = data.autoRoadAddress
										+ extraRoadAddr;
								document.getElementById('guide').innerHTML = '(예상 도로명 주소 : '
										+ expRoadAddr + ')';

							} else if (data.autoJibunAddress) {
								var expJibunAddr = data.autoJibunAddress;
								document.getElementById('guide').innerHTML = '(예상 지번 주소 : '
										+ expJibunAddr + ')';

							} else {
								document.getElementById('guide').innerHTML = '';
							}
						}
					}).open();
		}

		function registerCheckFunction() {
			var userid = $('#checklogintext').val();
			console.log(userid);
			$.ajax({
				type : "POST",
				url : "LoginCheck",
				data : {
					"userid" : userid
				},

				success : function(result) {
					if (result == '1') {

						$('#checkMessage').html('사용가능합니다. ');
						$('#checkType').attr('class');

						checkid=true;
					} else {
						$('#checkMessage').html('아이디가 이미 사용중입니다.');
						$('#checkType').attr('class');
						checkid=false;
					}
					$('#checkModal').modal("show");
				}
			})
			checktest=false;
		}

		jQuery(document)
				.ready(
						function($) {
							$('#MeuCarousel').carousel({
								interval : 4000
							});

							var clickEvent = false;
							$('#MeuCarousel')
									.on('click', '.nav a', function() {
										clickEvent = true;
										$('.nav li').removeClass('active');
										$(this).parent().addClass('active');
									})
									.on(
											'slid.bs.carousel',
											function(e) {
												if (!clickEvent) {
													var count = $('.nav')
															.children().length - 1;
													var current = $('.nav li.active');
													current.removeClass(
															'active').next()
															.addClass('active');
													var id = parseInt(current
															.data('slide-to'));
													if (count == id) {
														$('.nav li')
																.first()
																.addClass(
																		'active');
													}
												}
												clickEvent = false;
											});

							var navListItems = $('div.setup-panel div a'), allWells = $('.setup-content'), allNextBtn = $('.nextBtn');

							allWells.hide();

							navListItems
									.click(function(e) {
										e.preventDefault();
										var $target = $($(this).attr('href')), $item = $(this);

										if (!$item.hasClass('disabled')) {
											navListItems.removeClass(
													'btn-primary').addClass(
													'btn-default');
											$item.addClass('btn-primary');
											allWells.hide();
											$target.show();
											$target.find('input:eq(0)').focus();
										}
									});

							allNextBtn
									.click(function() {
										var curStep = $(this).closest(
												".setup-content"), curStepBtn = curStep
												.attr("id"), nextStepWizard = $(
												'div.setup-panel div a[href="#'
														+ curStepBtn + '"]')
												.parent().next().children("a"), curInputs = curStep
												.find("input[type='text'],input[type='url'],input[type='password']"), isValid = true;

										$(".form-group").removeClass(
												"has-error");
										for (var i = 0; i < curInputs.length; i++) {
											if (!curInputs[i].validity.valid) {
												isValid = false;
												$(curInputs[i]).closest(
														".form-group")
														.addClass("has-error");
											}
										}

										if (isValid)
											nextStepWizard.removeAttr(
													'disabled')
													.trigger('click');
									});

							$('div.setup-panel div a.btn-primary').trigger(
									'click');

						});
	</script>
</body>