<!-- 로그인창 -->

<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="../../top.jsp"%>

   <style type="text/css">
.form-signin
{
    max-width: 330px;
    padding: 15px;
    margin: 0 auto;
}
.form-signin .form-signin-heading, .form-signin .checkbox
{
    margin-bottom: 10px;
}
.form-signin .checkbox
{
    font-weight: normal;
}
.form-signin .form-control
{
    position: relative;
    font-size: 16px;
    height: auto;
    padding: 10px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
.form-signin .form-control:focus
{
    z-index: 2;
}
.form-signin input[type="text"]
{
    margin-bottom: -1px;
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
}
.form-signin input[type="password"]
{
    margin-bottom: 10px;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
}
.account-wall
{
    margin-top: 20px;
    padding: 40px 0px 20px 0px;
    background-color: #f7f7f7;
    -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
}
.login-title
{
    color: #555;
    font-size: 18px;
    font-weight: 400;
    display: block;
}
.profile-img
{
    width: 96px;
    height: 96px;
    margin: 0 auto 10px;
    display: block;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    border-radius: 50%;
}
.need-help
{
    margin-top: 10px;
}
.new-account
{
    display: block;
    margin-top: 10px;
}
   </style>


<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <h1 class="text-center login-title">WECAR 로그인</h1>
            <div class="account-wall">
                <img class="profile-img" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120"
                    alt="">
                <form class="form-signin" action="login1">
                <input name="id" type="text" class="form-control" placeholder="아이디" required autofocus>
                <input name="pw" type="password" class="form-control" placeholder="비밀번호" required>
                <button class="btn btn-lg btn-primary btn-block" type="submit">로그인</button>
                   
                <label class="checkbox pull-right">
                    <input type="checkbox" value="remember-me"> 아이디 기억하기
                </label>
<!--                 <br> -->
               
<!--             <label class="text-right"> -->
<!--                         <a href="#" >아이디/비밀번호 찾기</a> -->
<!--                 </label> -->
            
<!--             <div align = "right"> -->
<!--                <a href="#" >아이디/비밀번호 찾기</a><span class="clearfix"></span> -->
<!--                 <a href="#" class="pull-right need-help">Need help? </a><span class="clearfix"></span> --> 
<!--                 </div> -->
                </form>
            </div>
           <h4> <a href="#" class="text-center new-account">아이디/비밀번호 찾기 </a></h4>
           <h4> <a href="#" class="text-center new-account">회원가입하기 </a></h4>
        </div>
    </div>
</div>

<%@include file="../../bottom.jsp"%>