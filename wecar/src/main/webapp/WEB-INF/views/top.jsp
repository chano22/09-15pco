<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
        @import 'http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin';
        @import 'http://fonts.googleapis.com/css?family=Anton';

        body {
            min-height: 500px;
            background-color: #f3f3f3;
            color: #333;
            font: 13px/18px 'Open Sans', arial, sans-serif;
            -webkit-font-smoothing: antialiased;
        }

        .navbar-nav > li > a {
            padding-top: 15px;
            padding-bottom: 15px;
            line-height: 20px;
        }

        .top-bar {
            background-color: #fff;
            margin-bottom: 0px;
            width: 100%;
            height: 50px;
            webkit-box-shadow: rgba(0, 0, 0, 0.14902) 0px 2px 4px 0px;
            -moz-box-shadow: rgba(0, 0, 0, 0.14902) 0px 2px 4px 0px;
            box-shadow: rgba(0, 0, 0, 0.14902) 0px 2px 4px 0px;
        }

        .menu-item {
            line-height: 20px;
            color: #3f729b;
            font-weight: bold;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-size: 13px;
        }

        .menu-item-flexible {
            max-width: 230px;
            text-overflow: ellipsis;
            overflow: hidden;
            white-space: nowrap;
        }

        .web-logo {
            color: #3f729b;
            font-size: 18px;
            font-weight: 200;
            line-height: 1;
            padding-left: 0px;
        }

        @media (max-width: 768px) {
            .web-logo {
                padding-left: 15px;
            }
        }

        .header-profile {
            color: #3f729b;
            font-weight: bold;
            line-height: 1;
            max-width: 100px;
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden;
        }

        .navbar-nav > li > a.header-profile {
            padding-top: 10px;
            padding-bottom: 10px;
            line-height: 30px;
        }

        .user-name-box {
            margin-left: 5px;
            display: none;
        }

        @media (min-width:768px) {
            .navbar-nav > li > a.header-profile {
                padding-right: 5px;
            }

            .web-logo {
                padding-left: 15px;
            }
        }

        #bs-navbar {
            background-color: #fff;
        }

        @media (max-width:767px) {
            .navbar-nav > li > a.header-profile {
                max-width: none;
                text-overflow: inherit;
                white-space: inherit;
                overflow: inherit;
                padding-top: 5px;
                padding-bottom: 5px;
                line-height: 25px;
            }

            .menu-item {
                font-size: 14px;
            }

            .menu-item-flexible {
                max-width: none;
                text-overflow: inherit;
                overflow: inherit;
                white-space: inherit;
            }

            .user-name-box {
                font-size: 14px;
                display: inline-block;
            }

            .navbar-nav > li {
                background-color: #eee;
            }

            #bs-navbar {
                background-color: #eee;
                font-size: 15px;
                webkit-box-shadow: rgba(0, 0, 0, 0.14902) 0px 2px 4px 0px;
                -moz-box-shadow: rgba(0, 0, 0, 0.14902) 0px 2px 4px 0px;
                box-shadow: rgba(0, 0, 0, 0.14902) 0px 2px 4px 0px;
            }
        }

        .navbar-toggle .icon-bar {
            background-color: #3f729b;
        }

        .navbar-toggle {
            padding-right: 0px;
        }

        @media (max-width: 768px) {
            .top-bar {
                padding-left: 0px;
            }

            .navbar-brand {
            }
        }

        .container > .navbar-header,
        .container-fluid > .navbar-header,
        .container > .navbar-collapse,
        .container-fluid > .navbar-collapse {
            margin-left: 0px;
            margin-right: 0px;
        }
        
        /*
            slide 스타일
        */

        
.carousel .item img{ 
width:100%;
height:420px;
}

.carousel .nav {
text-align:center;
margin:0 auto;
max-width:1140px;
}


.carousel .nav li img {
text-align:center;
margin:0 auto;
padding:15px 0 0 0;

}


.carousel h2{
text-transform:uppercase;
font-size:18px;
text-align:center;
}



.carousel li.active {
    background: #90d91b;
}

.nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
    color:#fff;
    background: #90d91b;
}


.nav-pills a:hover{
    z-index:99;
    background: #90d91b;
    color:#fff;
    position: relative;
    cursor:pointer;
}


li:hover{
    z-index:99;
    background: #90d91b;
    color:#fff;
    position: relative;
    cursor:pointer;
}
 

li.active>a{
    z-index:99;
    background: #90d91b;
    color:#fff;
    position: relative;
    top: -20px;
    cursor:pointer;

}


.nav-pills>li.active>a{
    z-index:99;
    background: #90d91b;
    color:#fff;
    position: relative;
    top: -20px;
    cursor:pointer;
}


.carousel-control i {
    position: relative;
    top:50%;
    bottom: 0;
    left: 0;
    right:0;
    width: 100%;
    font-size: 90px;
    color:  #90d91b;
    text-align: center;
    text-shadow: none;
    filter: none;
    opacity: 1;
}


.carousel-caption {
    position: absolute;
    top:45%;
   z-index: 10;
    padding-top: 0px;
    padding-bottom: 0px;
    color: #fff;
}


 .carousel-caption h3 {
    font-size:88px;
    right: 0;
    top:0;
    left: 0;
    z-index: 10;
    color: #90d91b;
    text-align: center;
    text-shadow:  5px 5px 0px rgba(0,0,0,.9);
}
 .carousel {
     height:420px;
     border-bottom:10px solid #00d2ff;
    
 }
 .slide{
     height:420px;
 }
 .bottombody{
     width:500px;
     height:1140px;
     border:10px;
     margin-left:20%;
     margin-right:20%;

 }

 /*
     body적용


 */
         .hide-bullets {
list-style:none;
margin-left: -40px;
margin-top:20px;
}

         /*
             footer
         */
    .underfooter{
     height:300px;
     width:1920px;
     background-color:dimgrey;
 }
  .footer-menu > li{
       float:left;
       list-style:none;
    font-family:Dotum,tahaoma;
    font-size:12px;
    line-height:1.3em;
    color:#666;
    }
  .slide{
     margin-bottom:30px;
 }
 
  .carousel-control{
        
          opacity: 0;
  }
  .carousel-control.left,.carousel-control.right{
      background-image:none;
  }
  .top-wrapper{
          border-top:4px solid #00d2ff;
  }
  .top-wrapper > #MeuCarousel > .carousel-inner{
              border-bottom:4px solid #00d2ff;
  }

.select-first{
    color:#333;
}
.select-hr{
border-top : 1px solid #322323;
}
.hide-bullets > li
{
float:left;

    
} 
.hide-bullets > .col-sm-3.active > div{
    background-color:blue;
    
}
.hide-bullets > .col-sm-3 > div{
    background-color:gray;
    
}





.
/*
       <div id="MeuCarousel" class="carousel slide" data-ride="carousel">

    
       <div id="select-first"><h5>SMART SOCAR</h5></div>
                        <hr id="select-hr"/>
    
*/              
    </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
</script><script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
</head>
<body>
   <div class="top-wrapper">
        <header class="navbar navbar-static-top top-bar" id="top" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#bs-navbar" aria-controls="bs-navbar" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand web-logo" href="http://meaww.com" style="padding: 10px">SOCAR</a>
                </div>
                <nav id="bs-navbar" class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="useway" class="menu-item">서비스안내</a>
                        </li>
                        <li>
                            <a href="" class="menu-item">요금안내</a>
                        </li>
                        <li>
                            <a href="notice" class="menu-item">고객센터</a>
                        </li>
                        <li>
                            <a href="mypage" class="menu-item">마이페이지</a>
                        </li>
                        <li>
                            <a href="remain" class="menu-item">쏘카 찾기·예약</a>
                        </li>
                    </ul>
               
                    <ul class="nav navbar-nav navbar-right">
                    <c:choose>
                    	<c:when test="${dto.id != null}">
                    	<li><a><span class="glyphicon glyphicon-user"></span>${dto.username} 님</a> </li>
              			<li><a href="logout"><span class="glyphicon glyphicon-user"></span>로그아웃 </a></li>
                    	</c:when>
                    	<c:otherwise>
                    	<li><a href="join1"><span class="glyphicon glyphicon-user"></span> 회원가입</a></li>
                        <li><a href="login"><span class="glyphicon glyphicon-log-in"></span> 로그인</a></li>
                    	</c:otherwise>
                    </c:choose>
                    </ul>
                 </nav>
               
            </div>
        </header>