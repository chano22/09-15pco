<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:choose>
   <c:when test="${url==null}">
      <script type="text/javascript">
         alert("${msg}")
         history.back()
      </script>
   </c:when>
   <c:when test="${url=='close'}">
      <script type="text/javascript">
         alert("${msg}")
         self.close()
         opener.location.reload(); 
      </script>
   </c:when>
   <c:otherwise>
      <script type="text/javascript">
         alert("${msg}")
         location.href="/spring_mybatis/${url}"
      </script>
   </c:otherwise>
</c:choose>